# Assignment 5: Full Bodied 

**Due: Monday, November 4, 2019 at 10:00 AM**

We're now at the final stage of designing the shape. This assignment
continues on the work in class on the 28th, when we settled on a silhouette
of the roof shape and three body designs that fit that silhouette.  You
will also write down our initial concerns, informed by building these
designs.

There are two item due. You will work in 3 groups to build designs
and individually write down engineering concerns.

## Part 1: Engineering Concerns

Go to the [Google Sheet sent out on the class mailing 
list](https://docs.google.com/spreadsheets/d/1zPCNg-i_u9APaeI-DbcXkJ12ypTwWgWCbL0YnXESCog/edit?usp=sharing). 
Sign up for one problem area as primary
(lead) and one problem area as secondary (contributor). For both problem areas
you signed up for, write down one concern (with your name) for each of the 7 topics:

  1. Safety	
  1. Reliability	
  1. Environment	
  1. Power	
  1. Weight	
  1. Effort/Time	
  1. Cost

You may want to do this part after doing part 1, as building a design will
likely inform you greatly on what might go wrong.


## Part 2: Build a Body with Wings

We've broken up the class into three groups. Each group will take one of the
3 finalist variations of the roof rhombus design and build a 1 or more complete 
versions of it, including body materials. Don't worry about the internal materials
and structure: the goal here is to see what the material looks like on the outside.
For example, building a rigid body with Duron then coating it with copper tape is
acceptable for a copper body. Note, however, that the body must be durable and rigid
enough to be hung (details below).

Your shape must:

  1. Be of the same size as a piece that would be hung in the stairwell: no scale models.
  1. Consist of a body following (or a slight variation of) the shape assigned to your group.
  1. Have acrylic wings with dichroic film.
  1. Have the wings attached rigidly to the body.
  1. Have an eye hook or other method of attaching a string to hang the shape with. We
     will have PVC poles with strings on them in class on Monday. We will attach the
     pieces to these poles and look at the shapes while people hold them in different
     areas of the stairwell. Be sure that the attachment balances the shape properly.
     This should be approximately a 1/4" bolt with a 1/2" eye.
  1. Have a body whose external appearance is of a proposed final material (appearance
     matters). This can be milky acrylic, metal, wood, or whatever you choose. 

Please note that the need to hang the shape, combined with rigidly attaching the wings,
means that the body will need to to be structurally more sound than just some tape or
glue.

In class on the 4th, we will select one of these designs to work on for the EE125
event, in which we will improve it in five ways (based on our 
subgroup signups):
  
  1. Make it structurally more rigid and safe for people to touch and look at.
  1. Make it able to move/flap its wing shapes; this will require making a motor
     mount, running power, and placing a Feather board inside to control the motion.
  1. Mount it on a stand, run power and a data cable (for the Feather) to it.
  1. Light the wings (and, potentially, the body).
  1. Program the Feather with motions and light display patterns.

The groups are as follows:

  - Shape 1: Andrea Stein, Sean Konz, and Mihir Garimella
  - Shape 2: Kelly Woo, Tim Vrakas, Michal Adamkiewicz, and Will Thompson
  - Shape 3: Claire Huang, David Mendoza, and Lee Marom 

Finally, if you need to purchase some small supplies (fasteners, etc.), please
keep all of your receipts. You have a $50 budget. If you need more expensive
materials, please email the staff list. Finally, if you want materials for the 
exterior that have some lead time, decide on this quickly so you'll have them.

## Handing In

Bring your completed shape to class on Monday, November 4th. Before class, have
one person from your group sent a short (500 word or so) email to the staff email list 
explaining:

  - Who in the group did what?
  - What was harder than you expected?
  - What would you do differently if you were to build it again or needed to build 100? 

 


