# Assignment 1: CircuitPython, LEDs and Servos

*Written by Philip Levis*

**Due: Tuesday, October 1, 2019 at 10:00 PM**

One important distinguishing characteristic of embedded systems is
they interact with the physical world. They blink lights, control
motors, and read sensors. Because each element of the art installation
will potentially do all of these things, and we want it to be programmable,
we're going to start with this core part of its design.

Over these first three weeks, you're going to get a small 
embedded computer up and running and program it to do
two key things the art piece will require: actuate a motor and light up
LEDs. You'll see that both of these have some timing requirements, and
that their interaction may require a careful bit of system design. The
key takeaway is understanding that the physical world imposes constraints
and requirements on what your software can do and how it can do it.
Having a good sense of what's easy and what's hard will be very valuable
when we make design decisions later on.

There are questions interspersed with the assignment. You complete the
assignment by emailing answers to these questions to the course staff.

## Goals
When you're done with this assignment, you should

- be able to program your Feather with Python,

- know how to use a breadboard,

- be able to control a servo motor, and

- be able to program NeoPixel LEDs.

## Basic part: Feather board

As we discussed in class, the Feather board has a ton of useful parts on it,
including USB support and lots of different buses you can
attach external components to. It also has a whopping 192kB of RAM!
This might seem tiny compared to a laptop, but as you'll see in embedded
systems we tend to need much less RAM.  Later in the quarter, we'll 
transition to using a custom Stanford board, called Feather. In this 
assignment, though, we're going to be very simple. We're going to use 
just two GPIO (general purpose input output) pins as well as 5V power and 
ground.

As a first step, take your Feather board and insert it into your
breadboard.  If you've never used a breadboard before, have no fear:
they are quite simple, and [How to Use a
Breadboard](https://www.losant.com/blog/how-to-use-a-breadboard) has a
great summary. You want to insert it so that each row of pins is on a
different side of the board, like so:

![Feather board in breadboard](feather.jpg)

Sticking your Feather in the breadboard serves two purposes. First,
it will make it easier to attach wires to it, especially when you want
to attach more than one wire to the same pin. Second, it protects the
pins so they won't be easily bent if the board is banged up a bit.

Using your USB micro cable, connect your Feather to your computer. The
firmware (low-level code running directly on the hardware) controlling
the USB port tells your computer that the board is two kinds of USB
devices.  First, it's a serial port: this lets you type Python
directly on the board using a standard read-eval-print-loop (REPL). It
also lets you read `print()` statements. Second, it's a storage
device: this lets you copy over Python files that will run
automatically when the Feather boots.

Your board should appear as a CIRCUITPY drive on your computer. [The
CIRCUITPY Drive] explains the files you see there and what they do.
In summary, there's a bunch of library files that allow you to
interact with the Feather hardware from Python (e.g., read sensors,
control LEDs, read and write pins). The Feather differs slightly from
standard CircuitPython. In standard CircuitPython, there a file,
`code.py`, which the firmware runs when it boots. The Feather loads
and runs `main.py`. If you copy a new `main.py` to CIRCUITPY, it'll
run.

First thing, you'll want to copy over a bunch of library files that
give you access to the Feather's features. Download 
[circuitpy-libs.zip](circuitpy-libs.zip), unzip it, and copy the 
`lib` folder to CIRCUITPY. Next, download [feather.py](feather.py).
Take a look at `feather.py`. It looks like this:

```
# general blink routine for neopixel
# should work on all boards with an "NEOPIXEL" pin defined in firmware
#
# M.Holliday

import time
import board, neopixel, digitalio

pixel = neopixel.NeoPixel(board.NEOPIXEL, 1, brightness=0.5, auto_write=False)

def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos*3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos*3)
        g = 0
        b = int(pos*3)
    else:
        pos -= 170
        r = 0
        g = int(pos*3)
        b = int(255 - pos*3)
    return (r, g, b)


def rainbow_cycle(wait):
    for j in range(255):
        pixel_index = (256 // 1) + j
        pixel[0] = wheel(pixel_index & 255)
        pixel.show()
        time.sleep(wait)

######################### MAIN LOOP ##############################
while True:
     rainbow_cycle(0.01)
```

If you are not familiar with Python, there are lots of online
resources about it: ask the instructors if you need some guidance.


Copy `feather.py` to CIRCUITPY, renaming it to `main.py`.
You'll see that the LED on the board is cycling through colors. 
The key functions are `wheel` and
`rainbow_cycle`. Read them. The LED on board can display any color. It
does so by having three separate LEDs (red, green, blue, or RGB). By
adjusting the ratio of the brightness of those three, it can display
any color. This is because of [the physiology of the human
eye](https://en.wikipedia.org/wiki/RGB_color_model#Physical_principles_for_the_choice_of_red,_green,_and_blue):
RGB LEDs won't look right to a [mantis peacock
shrimp](https://www.livescience.com/42797-mantis-shrimp-sees-color.html),
for example.

The statement `pixel[0] = ` assigns a future RGB value to the one
pixel, while `pixel.show()` actually programs it. If you set
`auto_write=True` then assigning also programs.

1. What does the `wheel` function do?
1. What does the `rainbow_cycle` function do?
1. What are the units of the argument to `time.sleep()`?
1. Lower the brightness to 0.1. If you slow down the cycle, can you see the individual color changes? If not, how big a step in (0, 255) do you have to take for the change to be visible?

## Output and the Command Line

[Connecting to the Serial
Console](https://learn.adafruit.com/welcome-to-circuitpython/kattni-connecting-to-the-serial-console)
has instructions on how to connect to the Feather's serial
console. There's also [Advanced Serial Console on Mac and
Linux](https://learn.adafruit.com/welcome-to-circuitpython/advanced-serial-console-on-mac-and-linux)
if you have a Mac or Linux. This guide tells you how to figure out the
name to use to refer to the serial port. You want to use the `screen`
program to talk to the Feather. For example, if your Feather is
`/dev/tty.usbmodem141221`, then you want to type `screen
/dev/tty.usbmodem141221 115200`. The parameter `115200` tells
`screen` how fast the serial port is; USB serial ports are usually
at 115,200 bits per second.
If you are running Windows, refer to [Advanced Serial Console on
Windows](https://learn.adafruit.com/welcome-to-circuitpython/advanced-serial-console-on-windows).

Make a copy of `main.py` on your local drive, and modify it to add a
print statement in the main loop. Copy it over to CIRCUITPY (use the
command line, don't drag and click!) and connect to your Feather's
serial console. You should see the output.

Let's manually control the LED. Type control-C to enter the
REPL. Cribbing from the start of `main.py`, create a NeoPixel and
program it to be `(0, 10, 10)`, which is a dim cyan color. Try setting
the brightness to 255: `(0, 255, 255)`. You can barely look at
it. Finally, type `import main` to start your program again.

Generally speaking, the command line is useful for interactive
debugging.  But since you're manually entering code, it is easier to
make typos and other errors. A good workflow is usually to have most
of your code in files on CIRCUITPY, which you can import, then do just
a few simple things at the REPL.

## LED stick

Feather has a single NeoPixel LED on it. A NeoPixel LED is a tiny
microcontroller connected to a red, a green, and a blue LED. It
requires 5V of power. It has a single data in line and a single data
out line. You can send a NeoPixel a command to display a particular
RGB color. After it receives such a command, it ignores future display
commands and instead forwards them on its data out line. This allows
you to string them together. If you send N display commands, the first
one will set the first NeoPixel. The first NeoPixel will ignore the
second command and forward it to the second NeoPixel. You can also
send a command that tells the NeoPixel to start listening again: it
forwards this command along. So with a single command you can get the
chain of NeoPixels ready for a new display.

Don't worry about what these commands look like: we'll dig into them
in the second assignment.

Connect your LED stick to your Feather. The first step is to solder
wires onto the pads, so you can plug it into your bread board. Be
careful with colors: solder a red wire to 5VDC, a black wire to GND,
and a yellow wire to DIN. Use male jumper wires so you can plug them
into the breadboard. If you've never stripped wires or soldered
before, you get to learn -- this is a very simple solder job since the
pieces are big. Matt can help you get started. Finally, be sure to
heat shrink the wires onto the board to protect the joint. Otherwise
the wires will quickly break. It's OK if your heat shrink covers the
first LED. When you're done it should look something like this:

![LED stick with soldered wires](led-stick.jpg)

You need to give the stick 5V power and ground. Find the pin labeled
GND: if the USB connector is pointing up, it's the fourth one down on
the left (longer) row. Find the pin labeled USB: it's the fourth one
down on the right (shorter) row. USB provides 5V, so we can just tap
into this power. USB limits how much you can draw, so we can't power
hundreds of LEDs this way, but 8 is no problem.

One way to give power to the stick is to just plug into the USB and
GND rows on the breadboard. This is messy, though: we're potentially
going to need to provide power to multiple peripherals, and it can get
tight plugging them all into these rows. Instead, breadboards provide
power and ground columns: unlike most of the breadboard, where each
row is connected, these columns are connected. Using a red
male-to-male jumper wire, connect USB to the power rail (+). Using a
black male-to-male jumper wire, connect GND to the ground rail (-).
Then, plug your NeoPixel stick into the power and ground rails.

Now the stick is powered, but it also needs to receive commands from
the Feather. Connect the signal wire to pin A0 (or another pin you
prefer). Modify your copy of `main.py` to create an array of 8
NeoPixels on this pin:

```
stick = neopixel.NeoPixel(board.A0, 8, brightness=0.5, auto_write=False)
```

Then modify `rainbow_cycle` to write to all 8 pixels. Copy `main.py`
onto CIRCUITPY and watch them change colors!

![LED stick lit up](led-stick-lit.jpg)

## Servo motor

The elements of the art piece aren't only going to light up, they're
also going to move. So we need to be able to control a motor.  There
are many kinds of motors out there, including brushed, brushless,
stepper, geared, and servo. We'll explore these more in weeks 4-5 when
we explore which kind of motor we'll want to use in the elements and
how strong they'll need to be. We're going to start with a servo
motor.

Servo motors allow you to precisely control their physical
position. Unlike a standard motor, which spins (e.g., as in a drill or
ceiling fan), a servo motor has a small angle of travel and holds a
position. They're used in all kinds of applications. The small ones
we're using come from remote-control (RC) vehicles, e.g., to control
the flaps of an RC airplane.

Because different types of motors provide different mechansisms
(rotation, stepping, position), they have different interfaces. A
standard brushless motor, for example, just needs two wires. Apply a
voltage in one direction and it turns clockwise. A higher voltage
turns faster. Apply a voltage in the opposite direction and it turns
counter clockwise.

The standard interface to servo motor is three wires: power, ground,
and signal. For your HS-311, red is power, black is ground, and yellow
is signal:

![HS-311 wires](hs-311.jpg)

What signal does the motor expect? It's something called pulse width
modulation (PWM), which means it expects a periodic digital signal of
a square wave. [The duration of the square wave controls the
motor](https://en.wikipedia.org/wiki/Servo_control). A short square
pulse positions the motor in the counter-clockwise direction.  A
medium length square pulse positions the motor at center. A long
square pulse positions the motor in the clockwise direction.

The motors we're using expect a pulse every 20 milliseconds. They also
expect the pulse to be between 0.5 and 2.5 milliseconds long.  A 0.5
millisecond pulse turns the motor to -90 degrees; a 1.5 millisecond
pulse turns the motor to 0 degrees; a 2.5 millisecond pulse turns the
motor to 90 degrees.

Take three male-to-male jumper wires: one black, one red, one yellow.
Plug them into the corresponding pins on the HS311 connector, then
into your breadboard: black to ground, red to power, and yellow to a
GPIO pin, e.g. A1.

Make your pin an input/output pin that you can control:

```
servo = digitalio.DigitalInOut(board.A1)
servo.direction = digitalio.Direction.OUTPUT
```

How you can set the pin high by setting `servo.value = True` and
low by setting `servo.value = False`.

### Observing your signal

Part of the challenge of servo motors is they have pretty precise
timing. You need to send a pulse every 20 milliseconds, and the length
of the pulse varies by only 2 milliseconds. The shortest pulse is 500
microseconds. On a low-power microcontroller like the one Feather has,
500 microseconds is 60,000 instructions. This is a lot, but recall that
we're using Python. Python is a scripting language intended for easy
programming, not fast code or precise timing. How are we going to
control the durations of our pulses?

The first step to doing this is to actually observe the pulses and
measure their timing. Get a Saleae logic analyzer and download the
[Logic
program](https://support.saleae.com/logic-software/latest-beta-release)
from Saleae. These are fantastic little tools that let you see exactly
what your microcontroller is doing.

Add a jumper wire to your data pin through the breadboard. Connect this to 
pin 0 of the Saleae.  Connect the pin 0 ground pin to ground. Now you're 
ready to see what your microcontroller is doing.

Unplug the servo motor from your data pin. Write an infinite loop that
toggles the pin up and down. 

1. Given that the processor runs at 120MHz, each instruction takes
about 8 nanoseconds. If this code were written in C, you'd expect to
see the pin going up and down every 48-64 nanoseconds or so: [2 clock
cycle to write high, 2 to write low, then 2-4 cycles for the
unconditional branch to the top of the
loop](http://infocenter.arm.com/help/index.jsp?topic=/com.arm.doc.ddi0439b/CHDDIGAC.html).
How long do you think it will take in Python? 

Start up Logic and click on the arrows next to the Start button. Configure 
it to sample pin only 0 digitally, at 100Ms/s, for 1 second.  Click Start.

1. How long are the high and low periods of your pin?
1. Is one longer than the other? Why?
1. How does this compare with your earlier guess?


### Timing a loop

Change your loop so that it sends a 1.5ms high pulse every 20ms. Two
functions that will be helpful are `time.monotonic()`, which
returns the current time in seconds and
`microcontroller.delay_us()`, which delays a certain number of
microseconds. There's one catch with `time.monotonic()`, though:
while it is a floating point number, it only reports at the granularity
of milliseconds! So you can see output such as:

```
time = 1.123
time = 1.123
time = 1.123
time = 1.124
```

You'll quickly notice that these delays and timings aren't very precise. E.g.,
calling `microcontroller.delay_us(1000)` most likely won't delay for exactly 1ms,
in part because of clock accuracy and in part because of Python's overheads.
Tweak your constants so you get a signal as close to 1.5ms every 20ms as you can.

Plug your servo's data line back in.

1. Where does your servo motor move to with a 1.5ms pulse?
1. Adjust the length of your pulse so the motor moves to 0 degrees. Looking with the Saleae logic
analyzer, how long a pulse is this?

## Handing in

Please send an email to the staff list with subject "Assignment 1"
which has your answers to all of the above questions. Please send it
in the body of an email: do not send attachments, Google doc links,
etc. If you received significant help from someone besides the course
staff, please describe the help you received at the top of your
handin.




