EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	7000 2950 7000 3050
Connection ~ 7000 3050
Wire Wire Line
	7000 3050 7000 3150
Connection ~ 7000 3150
Wire Wire Line
	7000 3150 7000 3250
Connection ~ 7000 3250
Wire Wire Line
	7000 3250 7000 3350
Connection ~ 7000 3350
Wire Wire Line
	7000 3350 7000 3450
Connection ~ 7000 3450
Wire Wire Line
	7000 3450 7000 3550
Connection ~ 7000 3550
Wire Wire Line
	7000 3550 7000 3650
Connection ~ 7000 3650
Wire Wire Line
	7000 3650 7000 4300
Wire Wire Line
	5650 4300 5250 4300
Wire Wire Line
	4950 4300 4950 4155
Connection ~ 5650 4300
$Comp
L Device:R_Small R35
U 1 1 61797531
P 5650 4050
AR Path="/5E46B36B/61797531" Ref="R35"  Part="1" 
AR Path="/5E4888F1/61797531" Ref="R8"  Part="1" 
AR Path="/5E48C025/61797531" Ref="R15"  Part="1" 
F 0 "R8" H 5709 4096 50  0000 L CNN
F 1 "5k" H 5709 4005 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5650 4050 50  0001 C CNN
F 3 "~" H 5650 4050 50  0001 C CNN
	1    5650 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 3050 5000 3850
Wire Wire Line
	5000 3850 5650 3850
Wire Wire Line
	5650 3850 5650 3950
$Comp
L Device:R_Small R33
U 1 1 6179916A
P 5250 4100
AR Path="/5E46B36B/6179916A" Ref="R33"  Part="1" 
AR Path="/5E4888F1/6179916A" Ref="R6"  Part="1" 
AR Path="/5E48C025/6179916A" Ref="R13"  Part="1" 
F 0 "R6" H 5309 4146 50  0000 L CNN
F 1 "30.1k" H 5309 4055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5250 4100 50  0001 C CNN
F 3 "~" H 5250 4100 50  0001 C CNN
	1    5250 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5250 3150 5250 4000
Wire Wire Line
	7050 2850 7050 2350
Wire Wire Line
	7050 2350 4450 2350
Wire Wire Line
	4450 2350 4450 2910
$Comp
L Device:R_Small R30
U 1 1 6179DDCB
P 4450 3010
AR Path="/5E46B36B/6179DDCB" Ref="R30"  Part="1" 
AR Path="/5E4888F1/6179DDCB" Ref="R3"  Part="1" 
AR Path="/5E48C025/6179DDCB" Ref="R10"  Part="1" 
F 0 "R3" H 4509 3056 50  0000 L CNN
F 1 "10k" H 4509 2965 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4450 3010 50  0001 C CNN
F 3 "~" H 4450 3010 50  0001 C CNN
	1    4450 3010
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 3110 4450 3650
Wire Wire Line
	4450 2350 4150 2350
Wire Wire Line
	4150 2350 4150 2345
Connection ~ 4450 2350
$Comp
L Device:C C38
U 1 1 6179FCE4
P 4150 2495
AR Path="/5E46B36B/6179FCE4" Ref="C38"  Part="1" 
AR Path="/5E4888F1/6179FCE4" Ref="C7"  Part="1" 
AR Path="/5E48C025/6179FCE4" Ref="C15"  Part="1" 
F 0 "C7" H 4265 2541 50  0000 L CNN
F 1 "1uF" H 4265 2450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4188 2345 50  0001 C CNN
F 3 "~" H 4150 2495 50  0001 C CNN
	1    4150 2495
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 2645 4150 2850
Wire Wire Line
	5900 2550 5450 2550
Wire Wire Line
	6050 3750 6050 3900
Wire Wire Line
	7650 2150 8100 2150
$Comp
L Device:C C40
U 1 1 617A919F
P 8100 2450
AR Path="/5E46B36B/617A919F" Ref="C40"  Part="1" 
AR Path="/5E4888F1/617A919F" Ref="C9"  Part="1" 
AR Path="/5E48C025/617A919F" Ref="C17"  Part="1" 
F 0 "C9" H 8215 2496 50  0000 L CNN
F 1 "22uF" H 8215 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8138 2300 50  0001 C CNN
F 3 "~" H 8100 2450 50  0001 C CNN
	1    8100 2450
	1    0    0    -1  
$EndComp
$Comp
L Device:C C41
U 1 1 617A9B48
P 8550 2450
AR Path="/5E46B36B/617A9B48" Ref="C41"  Part="1" 
AR Path="/5E4888F1/617A9B48" Ref="C10"  Part="1" 
AR Path="/5E48C025/617A9B48" Ref="C18"  Part="1" 
F 0 "C10" H 8665 2496 50  0000 L CNN
F 1 "10uF" H 8665 2405 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 8588 2300 50  0001 C CNN
F 3 "~" H 8550 2450 50  0001 C CNN
	1    8550 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	8100 2150 8100 2300
Wire Wire Line
	8100 2150 8550 2150
Wire Wire Line
	8550 2150 8550 2300
Connection ~ 8100 2150
Wire Wire Line
	8100 2600 8100 2700
Wire Wire Line
	8100 2700 8350 2700
Wire Wire Line
	8550 2700 8550 2600
Wire Wire Line
	8350 2700 8350 2800
Connection ~ 8350 2700
Wire Wire Line
	8350 2700 8550 2700
$Comp
L Device:L L5
U 1 1 617AFE4D
P 5600 1300
AR Path="/5E46B36B/617AFE4D" Ref="L5"  Part="1" 
AR Path="/5E4888F1/617AFE4D" Ref="L2"  Part="1" 
AR Path="/5E48C025/617AFE4D" Ref="L3"  Part="1" 
F 0 "L2" V 5790 1300 50  0000 C CNN
F 1 "1.3uH" V 5699 1300 50  0000 C CNN
F 2 "light_footprints:1_3_MH_Inductor" H 5600 1300 50  0001 C CNN
F 3 "~" H 5600 1300 50  0001 C CNN
	1    5600 1300
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5100 3450 5100 1850
$Comp
L Device:R_Small R32
U 1 1 617B9003
P 5100 1700
AR Path="/5E46B36B/617B9003" Ref="R32"  Part="1" 
AR Path="/5E4888F1/617B9003" Ref="R5"  Part="1" 
AR Path="/5E48C025/617B9003" Ref="R12"  Part="1" 
F 0 "R5" H 5159 1746 50  0000 L CNN
F 1 "8.2k" H 5159 1655 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5100 1700 50  0001 C CNN
F 3 "~" H 5100 1700 50  0001 C CNN
	1    5100 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5100 1600 5100 1300
Connection ~ 5100 1300
Wire Wire Line
	5100 1300 5450 1300
Wire Wire Line
	3000 1300 3150 1300
$Comp
L Device:C C34
U 1 1 617BC887
P 3150 1600
AR Path="/5E46B36B/617BC887" Ref="C34"  Part="1" 
AR Path="/5E4888F1/617BC887" Ref="C3"  Part="1" 
AR Path="/5E48C025/617BC887" Ref="C11"  Part="1" 
F 0 "C3" H 3265 1646 50  0000 L CNN
F 1 "47uF" H 3265 1555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3188 1450 50  0001 C CNN
F 3 "~" H 3150 1600 50  0001 C CNN
	1    3150 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C35
U 1 1 617BCEBB
P 3550 1600
AR Path="/5E46B36B/617BCEBB" Ref="C35"  Part="1" 
AR Path="/5E4888F1/617BCEBB" Ref="C4"  Part="1" 
AR Path="/5E48C025/617BCEBB" Ref="C12"  Part="1" 
F 0 "C4" H 3665 1646 50  0000 L CNN
F 1 "47uF" H 3665 1555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3588 1450 50  0001 C CNN
F 3 "~" H 3550 1600 50  0001 C CNN
	1    3550 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 1450 3150 1300
Connection ~ 3150 1300
Wire Wire Line
	3150 1300 3550 1300
Wire Wire Line
	3550 1450 3550 1300
Connection ~ 3550 1300
Wire Wire Line
	3150 1750 3150 1950
Wire Wire Line
	3150 1950 3185 1950
Wire Wire Line
	3550 1950 3550 1750
Wire Wire Line
	3350 1950 3350 2100
Connection ~ 3350 1950
Wire Wire Line
	3350 1950 3550 1950
Wire Wire Line
	4900 3350 4900 2150
Wire Wire Line
	4900 1950 4250 1950
Connection ~ 3550 1950
Wire Wire Line
	4250 1950 4250 1850
Connection ~ 4250 1950
Wire Wire Line
	4250 1950 3950 1950
$Comp
L Device:R_Small R29
U 1 1 617C8140
P 4250 1750
AR Path="/5E46B36B/617C8140" Ref="R29"  Part="1" 
AR Path="/5E4888F1/617C8140" Ref="R2"  Part="1" 
AR Path="/5E48C025/617C8140" Ref="R9"  Part="1" 
F 0 "R2" H 4309 1796 50  0000 L CNN
F 1 "1.1k" H 4309 1705 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 4250 1750 50  0001 C CNN
F 3 "~" H 4250 1750 50  0001 C CNN
	1    4250 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4250 1650 4250 1550
Wire Wire Line
	4250 1550 4900 1550
Wire Wire Line
	4900 1550 4900 1850
Wire Wire Line
	4900 1850 5100 1850
Connection ~ 5100 1850
Wire Wire Line
	5100 1850 5100 1800
$Comp
L Device:C C36
U 1 1 617CFABB
P 3750 2500
AR Path="/5E46B36B/617CFABB" Ref="C36"  Part="1" 
AR Path="/5E4888F1/617CFABB" Ref="C5"  Part="1" 
AR Path="/5E48C025/617CFABB" Ref="C13"  Part="1" 
F 0 "C5" H 3865 2546 50  0000 L CNN
F 1 "22nF" H 3865 2455 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3788 2350 50  0001 C CNN
F 3 "~" H 3750 2500 50  0001 C CNN
	1    3750 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2350 3750 2150
Wire Wire Line
	3750 2150 4900 2150
Connection ~ 4900 2150
Wire Wire Line
	4900 2150 4900 1950
$Comp
L Device:C C37
U 1 1 617A12DB
P 3950 1600
AR Path="/5E46B36B/617A12DB" Ref="C37"  Part="1" 
AR Path="/5E4888F1/617A12DB" Ref="C6"  Part="1" 
AR Path="/5E48C025/617A12DB" Ref="C14"  Part="1" 
F 0 "C6" H 4065 1646 50  0000 L CNN
F 1 "47uF" H 4065 1555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3988 1450 50  0001 C CNN
F 3 "~" H 3950 1600 50  0001 C CNN
	1    3950 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 1300 3950 1300
Wire Wire Line
	3950 1750 3950 1950
Connection ~ 3950 1950
Wire Wire Line
	3950 1950 3550 1950
Wire Wire Line
	3950 1450 3950 1300
Connection ~ 3950 1300
Wire Wire Line
	5650 4150 5650 4300
Wire Wire Line
	5250 4200 5250 4300
Connection ~ 5250 4300
Wire Wire Line
	5250 4300 4950 4300
Wire Wire Line
	5750 1300 6150 1300
Wire Wire Line
	3950 1300 5100 1300
Wire Wire Line
	4450 3650 5550 3650
Wire Wire Line
	5550 3450 5100 3450
Wire Wire Line
	5550 3350 4900 3350
Wire Wire Line
	5550 3250 3750 3250
Wire Wire Line
	5550 3150 5250 3150
Wire Wire Line
	5550 3050 5000 3050
Wire Wire Line
	4950 2950 5550 2950
$Comp
L Device:C C39
U 1 1 6184B0F4
P 5880 2075
AR Path="/5E46B36B/6184B0F4" Ref="C39"  Part="1" 
AR Path="/5E48C025/6184B0F4" Ref="C16"  Part="1" 
AR Path="/5E4888F1/6184B0F4" Ref="C8"  Part="1" 
F 0 "C8" V 5628 2075 50  0000 C CNN
F 1 "1uF" V 5719 2075 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5918 1925 50  0001 C CNN
F 3 "~" H 5880 2075 50  0001 C CNN
	1    5880 2075
	0    1    1    0   
$EndComp
$Comp
L Device:R R34
U 1 1 6184A6E3
P 5480 2075
AR Path="/5E46B36B/6184A6E3" Ref="R34"  Part="1" 
AR Path="/5E48C025/6184A6E3" Ref="R14"  Part="1" 
AR Path="/5E4888F1/6184A6E3" Ref="R7"  Part="1" 
F 0 "R7" V 5273 2075 50  0000 C CNN
F 1 "3.3" V 5364 2075 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 5410 2075 50  0001 C CNN
F 3 "~" H 5480 2075 50  0001 C CNN
	1    5480 2075
	0    1    1    0   
$EndComp
Wire Wire Line
	5550 2850 5330 2850
Wire Wire Line
	5330 2850 5330 2075
Wire Wire Line
	5630 2075 5730 2075
Wire Wire Line
	6030 2075 6150 2075
Wire Wire Line
	6150 1300 6150 2075
Connection ~ 6150 2075
Wire Wire Line
	6150 2075 6150 2800
$Comp
L Device:R R31
U 1 1 618750D5
P 4755 3295
AR Path="/5E46B36B/618750D5" Ref="R31"  Part="1" 
AR Path="/5E48C025/618750D5" Ref="R11"  Part="1" 
AR Path="/5E4888F1/618750D5" Ref="R4"  Part="1" 
F 0 "R4" H 4825 3341 50  0000 L CNN
F 1 "10K" H 4825 3250 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric" V 4685 3295 50  0001 C CNN
F 3 "~" H 4755 3295 50  0001 C CNN
	1    4755 3295
	1    0    0    -1  
$EndComp
Wire Wire Line
	4755 3550 4755 3445
Wire Wire Line
	5900 2550 5900 2800
Wire Wire Line
	4755 2630 4755 3145
Wire Wire Line
	5650 4300 7000 4300
Wire Wire Line
	4950 4155 3185 4155
Wire Wire Line
	3185 4155 3185 1950
Connection ~ 4950 4155
Wire Wire Line
	4950 4155 4950 2950
Connection ~ 3185 1950
Wire Wire Line
	3185 1950 3350 1950
Text HLabel 7650 2150 1    50   Input ~ 0
8Vin
Wire Wire Line
	6500 3650 7000 3650
Wire Wire Line
	6500 3550 7000 3550
Wire Wire Line
	6500 3450 7000 3450
Wire Wire Line
	6500 3350 7000 3350
Wire Wire Line
	6500 3250 7000 3250
Wire Wire Line
	6500 3150 7000 3150
Wire Wire Line
	6500 3050 7000 3050
Wire Wire Line
	6500 2950 7000 2950
Wire Wire Line
	6500 2850 7050 2850
Text HLabel 5450 2550 1    50   Input ~ 0
8Vin
Text HLabel 4755 2630 1    50   Input ~ 0
8Vin
Text HLabel 6050 3900 2    50   Input ~ 0
8Vin
Text HLabel 3000 1300 0    50   Output ~ 0
5Vout
Text HLabel 3350 2100 3    50   UnSpc ~ 0
GND
Text HLabel 8350 2800 3    50   UnSpc ~ 0
GND
Text HLabel 4150 2850 3    50   UnSpc ~ 0
GND
$Comp
L tail-rescue:Converter_DCDC_IS66066 U4
U 1 1 61B65628
P 6100 4050
AR Path="/5E46B36B/61B65628" Ref="U4"  Part="1" 
AR Path="/5E4888F1/61B65628" Ref="U1"  Part="1" 
AR Path="/5E48C025/61B65628" Ref="U2"  Part="1" 
F 0 "U1" H 6868 4921 50  0000 L CNN
F 1 "Converter_DCDC_IS66066" H 6868 4830 50  0000 L CNN
F 2 "light_footprints:IS66066_Annotated_Numbers" H 6100 4050 50  0001 C CNN
F 3 "" H 6100 4050 50  0001 C CNN
	1    6100 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3550 4755 3550
Wire Wire Line
	3750 2650 3750 3250
$EndSCHEMATC
