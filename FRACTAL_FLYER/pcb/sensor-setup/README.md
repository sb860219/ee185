# Schematic for motor/sensor test setup

This schematic shows the test setup Phil has used for getting the motors and sensors working on a Fractal Flyer. It shows the connections to the sensors and an external H-Bridge. 