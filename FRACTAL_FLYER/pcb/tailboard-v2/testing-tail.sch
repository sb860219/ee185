EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "FLIGHT Tail Board, EE125"
Date "2020-11-01"
Rev "2.1"
Comp "Stanford University"
Comment1 "Schematic for FLIGHT art installation tail board."
Comment2 "Tail board is on a Fractal Flyer, provides power and control."
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L tail-rescue:640456-3-dk_Rectangular-Connectors-Headers-Male-Pins J2
U 1 1 5E610559
P 4600 900
F 0 "J2" V 4450 800 50  0000 C CNN
F 1 "LED RIGHT" V 4950 800 50  0000 C CNN
F 2 "jst:JST_1x3" H 4800 1100 60  0001 L CNN
F 3 "" H 4800 1200 60  0001 L CNN
F 4 "CONN HEADER VERT 3POS 2.5MM" H 4800 1900 60  0001 L CNN "Description"
F 5 "JST Sales America Inc." H 4800 2000 60  0001 L CNN "Manufacturer"
F 6 "455-2248-ND" H 4600 900 50  0001 C CNN "Digikey PN"
F 7 "B3B-XH-A(LF)(SN)" H 4600 900 50  0001 C CNN "Manufacturer PN"
	1    4600 900 
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:GND-power #PWR09
U 1 1 5E62732A
P 4850 1150
F 0 "#PWR09" H 4850 900 50  0001 C CNN
F 1 "GND" H 4855 977 50  0000 C CNN
F 2 "" H 4850 1150 50  0001 C CNN
F 3 "" H 4850 1150 50  0001 C CNN
	1    4850 1150
	1    0    0    -1  
$EndComp
$Comp
L tail-rescue:10103594-0001LF-dk_USB-DVI-HDMI-Connectors J5
U 1 1 5E6292E8
P 6700 1950
F 0 "J5" H 6714 2673 50  0000 C CNN
F 1 "10103594-0001LF" H 6714 2582 50  0000 C CNN
F 2 "light_footprints:USB_Micro_B_Female_10103594-0001LF" H 6900 2150 60  0001 L CNN
F 3 "https://cdn.amphenol-icc.com/media/wysiwyg/files/drawing/10103594.pdf" H 6900 2250 60  0001 L CNN
F 4 "" H 6900 2350 60  0001 L CNN "Digi-Key_PN"
F 5 "" H 6900 2450 60  0001 L CNN "MPN"
F 6 "Connectors, Interconnects" H 6900 2550 60  0001 L CNN "Category"
F 7 "USB, DVI, HDMI Connectors" H 6900 2650 60  0001 L CNN "Family"
F 8 "https://cdn.amphenol-icc.com/media/wysiwyg/files/drawing/10103594.pdf" H 6900 2750 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/amphenol-icc-fci/10103594-0001LF/609-4050-1-ND/2350357" H 6900 2850 60  0001 L CNN "DK_Detail_Page"
F 10 "CONN RCPT USB2.0 MICRO B SMD R/A" H 6900 2950 60  0001 L CNN "Description"
F 11 "Amphenol ICC (FCI)" H 6900 3050 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6900 3150 60  0001 L CNN "Status"
F 13 "609-4050-1-ND" H 6700 1950 50  0001 C CNN "Digikey PN"
F 14 "10103594-0001LF" H 6700 1950 50  0001 C CNN "Manufacturer PN"
	1    6700 1950
	1    0    0    -1  
$EndComp
$Comp
L tail-rescue:GND-power #PWR018
U 1 1 5E62DEA6
P 7100 2650
F 0 "#PWR018" H 7100 2400 50  0001 C CNN
F 1 "GND" H 7105 2477 50  0000 C CNN
F 2 "" H 7100 2650 50  0001 C CNN
F 3 "" H 7100 2650 50  0001 C CNN
	1    7100 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 2150 7100 2150
$Comp
L tail-rescue:Conn_01x14_feather_right-Feather_connectors J?
U 1 1 5E42AD5B
P 9950 1600
AR Path="/5E41758F/5E42AD5B" Ref="J?"  Part="1" 
AR Path="/5E42AD5B" Ref="J9"  Part="1" 
F 0 "J9" H 10400 2300 50  0000 C CNN
F 1 "Conn_01x14" H 10050 800 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H 9950 1600 50  0001 C CNN
F 3 "~" H 9950 1600 50  0001 C CNN
F 4 "40-0518-10" H 9950 1600 50  0001 C CNN "Manufacturer PN"
F 5 "Aries Electronics" H 9950 1600 50  0001 C CNN "Manufacturer"
F 6 "A460-ND" H 9950 1600 50  0001 C CNN "Digikey PN"
F 7 "CONN SOCKET SIP 40POS GOLD" H 9950 1600 50  0001 C CNN "Description"
	1    9950 1600
	-1   0    0    -1  
$EndComp
Text Notes 8700 800  0    79   ~ 0
THRU-HOLE HEADERS\n    (SAMD51 I/O)
$Comp
L tail-rescue:GND-power #PWR?
U 1 1 5E42AD98
P 8000 2500
AR Path="/5E41758F/5E42AD98" Ref="#PWR?"  Part="1" 
AR Path="/5E42AD98" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 8000 2250 50  0001 C CNN
F 1 "GND" V 8005 2372 50  0000 R CNN
F 2 "" H 8000 2500 50  0001 C CNN
F 3 "" H 8000 2500 50  0001 C CNN
	1    8000 2500
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:GND-power #PWR?
U 1 1 5E42AD9F
P 8000 1300
AR Path="/5E41758F/5E42AD9F" Ref="#PWR?"  Part="1" 
AR Path="/5E42AD9F" Ref="#PWR019"  Part="1" 
F 0 "#PWR019" H 8000 1050 50  0001 C CNN
F 1 "GND" V 8005 1172 50  0000 R CNN
F 2 "" H 8000 1300 50  0001 C CNN
F 3 "" H 8000 1300 50  0001 C CNN
	1    8000 1300
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 900  4700 900 
Wire Wire Line
	4850 1150 4850 1100
Wire Wire Line
	4850 1100 4700 1100
$Comp
L tail-rescue:640456-3-dk_Rectangular-Connectors-Headers-Male-Pins J3
U 1 1 5E4888FB
P 4600 1800
F 0 "J3" V 4450 1700 50  0000 C CNN
F 1 "BODY" V 4950 1700 50  0000 C CNN
F 2 "jst:JST_1x3" H 4800 2000 60  0001 L CNN
F 3 "" H 4800 2100 60  0001 L CNN
F 4 "" H 4800 2200 60  0001 L CNN "Digi-Key_PN"
F 5 "" H 4800 2300 60  0001 L CNN "MPN"
F 6 "CONN HEADER VERT 3POS 2.5MM" H 4800 2800 60  0001 L CNN "Description"
F 7 "JST Sales America Inc." H 4800 2900 60  0001 L CNN "Manufacturer"
F 8 "455-2248-ND" H 4600 1800 50  0001 C CNN "Digikey PN"
F 9 "B3B-XH-A(LF)(SN)" H 4600 1800 50  0001 C CNN "Manufacturer PN"
	1    4600 1800
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:GND-power #PWR010
U 1 1 5E48890E
P 4850 2050
F 0 "#PWR010" H 4850 1800 50  0001 C CNN
F 1 "GND" H 4855 1877 50  0000 C CNN
F 2 "" H 4850 2050 50  0001 C CNN
F 3 "" H 4850 2050 50  0001 C CNN
	1    4850 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 1800 4700 1800
Wire Wire Line
	4850 2050 4850 2000
Wire Wire Line
	4850 2000 4700 2000
$Comp
L tail-rescue:640456-3-dk_Rectangular-Connectors-Headers-Male-Pins J4
U 1 1 5E48C02F
P 4600 2650
F 0 "J4" V 4450 2550 50  0000 C CNN
F 1 "LED LEFT" V 4950 2550 50  0000 C CNN
F 2 "jst:JST_1x3" H 4800 2850 60  0001 L CNN
F 3 "" H 4800 2950 60  0001 L CNN
F 4 "" H 4800 3050 60  0001 L CNN "Digi-Key_PN"
F 5 "" H 4800 3150 60  0001 L CNN "MPN"
F 6 "CONN HEADER VERT 3POS 2.5MM" H 4800 3650 60  0001 L CNN "Description"
F 7 "JST Sales America Inc." H 4800 3750 60  0001 L CNN "Manufacturer"
F 8 "455-2248-ND" H 4600 2650 50  0001 C CNN "Digikey PN"
F 9 "B3B-XH-A(LF)(SN)" H 4600 2650 50  0001 C CNN "Manufacturer PN"
	1    4600 2650
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:GND-power #PWR011
U 1 1 5E48C042
P 4850 2900
F 0 "#PWR011" H 4850 2650 50  0001 C CNN
F 1 "GND" H 4855 2727 50  0000 C CNN
F 2 "" H 4850 2900 50  0001 C CNN
F 3 "" H 4850 2900 50  0001 C CNN
	1    4850 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 2650 4700 2650
Wire Wire Line
	4850 2900 4850 2850
Wire Wire Line
	4850 2850 4700 2850
Text GLabel 4800 800  2    50   Input ~ 0
+5V_LED_1
Text GLabel 4800 1700 2    50   Input ~ 0
+5V_LED_2
Text GLabel 4800 2550 2    50   Input ~ 0
+5V_LED_3
Wire Wire Line
	8100 2200 8600 2200
Wire Wire Line
	8100 2100 8600 2100
Wire Wire Line
	8100 2000 8600 2000
Wire Wire Line
	8100 1900 8600 1900
Wire Wire Line
	8100 1800 8600 1800
Wire Wire Line
	8100 1700 8600 1700
Wire Wire Line
	8100 1600 8600 1600
Wire Wire Line
	8100 1500 8600 1500
Wire Wire Line
	8100 1400 8600 1400
Wire Wire Line
	8100 1200 8600 1200
Wire Wire Line
	10150 1000 10600 1000
Text Label 8300 2200 0    50   ~ 0
MISO
Text Label 8300 2100 0    50   ~ 0
MOSI
Text Label 8300 2000 0    50   ~ 0
SCK
Text Label 8300 1900 0    50   ~ 0
D31
Text Label 8200 1800 0    50   ~ 0
PWM_M1_A
Text Label 8200 1700 0    50   ~ 0
PWM_M1_B
Text Label 8200 1600 0    50   ~ 0
PWM_M2_A
Text Label 10200 1000 0    50   ~ 0
PWM_M2_B
Wire Wire Line
	8600 1300 8000 1300
Wire Wire Line
	8600 2500 8000 2500
Text GLabel 7250 1750 2    50   Input ~ 0
+5V_DIGITAL
Wire Wire Line
	6950 1750 7250 1750
Text Label 7050 1950 0    50   ~ 0
D+
Text Label 7050 1850 0    50   ~ 0
D-
Wire Wire Line
	6950 1850 7450 1850
Wire Wire Line
	6950 1950 7450 1950
NoConn ~ 6950 2050
Wire Wire Line
	6550 2550 6550 2600
Wire Wire Line
	6550 2600 7100 2600
Wire Wire Line
	7100 2600 7100 2650
Wire Wire Line
	7100 2150 7100 2600
Connection ~ 7100 2600
Text Label 8300 1500 0    50   ~ 0
CS_A3
Text Label 8300 1200 0    50   ~ 0
CS_A1
Text Label 8300 1400 0    50   ~ 0
CS_A2
NoConn ~ 8600 2300
NoConn ~ 8600 2400
NoConn ~ 8600 2600
NoConn ~ 10150 1200
NoConn ~ 10150 1300
NoConn ~ 10150 1400
NoConn ~ 10150 1100
NoConn ~ 10150 1800
NoConn ~ 10150 1900
NoConn ~ 10150 2000
NoConn ~ 10150 2100
NoConn ~ 10150 2200
NoConn ~ 10150 2300
Text Label 8300 1000 0    50   ~ 0
RESET
Wire Wire Line
	4700 1000 5250 1000
Wire Wire Line
	4700 1900 5250 1900
Wire Wire Line
	4700 2750 5250 2750
Wire Wire Line
	4800 2650 4800 2550
Wire Wire Line
	4800 1700 4800 1800
Wire Wire Line
	4800 800  4800 900 
Text Label 5250 1000 2    50   ~ 0
LED1_SIG
Text Label 5250 1900 2    50   ~ 0
LED2_SIG
Text Label 5250 2750 2    50   ~ 0
LED3_SIG
Text Label 10200 1500 0    50   ~ 0
LED1_SIG
Wire Wire Line
	10150 1500 10600 1500
Wire Wire Line
	10150 1600 10600 1600
Wire Wire Line
	10150 1700 10600 1700
Text Label 10200 1600 0    50   ~ 0
LED2_SIG
Text Label 10200 1700 0    50   ~ 0
LED3_SIG
$Comp
L tail-rescue:GND-power #PWR0102
U 1 1 5E58E9EF
P 1300 1800
F 0 "#PWR0102" H 1300 1550 50  0001 C CNN
F 1 "GND" H 1305 1627 50  0000 C CNN
F 2 "" H 1300 1800 50  0001 C CNN
F 3 "" H 1300 1800 50  0001 C CNN
	1    1300 1800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID4
U 1 1 5E7161A0
P 3200 1100
F 0 "FID4" H 3285 1146 50  0000 L CNN
F 1 "Fiducial" H 3285 1055 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 3200 1100 50  0001 C CNN
F 3 "~" H 3200 1100 50  0001 C CNN
	1    3200 1100
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID5
U 1 1 5E71CF17
P 3200 1450
F 0 "FID5" H 3285 1496 50  0000 L CNN
F 1 "Fiducial" H 3285 1405 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 3200 1450 50  0001 C CNN
F 3 "~" H 3200 1450 50  0001 C CNN
	1    3200 1450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID6
U 1 1 5E723BEE
P 3200 1800
F 0 "FID6" H 3285 1846 50  0000 L CNN
F 1 "Fiducial" H 3285 1755 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 3200 1800 50  0001 C CNN
F 3 "~" H 3200 1800 50  0001 C CNN
	1    3200 1800
	1    0    0    -1  
$EndComp
$Comp
L tail-rescue:Conn_01x17_feather_left-Feather_connectors J?
U 1 1 5E42AD55
P 8800 1800
AR Path="/5E41758F/5E42AD55" Ref="J?"  Part="1" 
AR Path="/5E42AD55" Ref="J8"  Part="1" 
F 0 "J8" H 8850 2700 50  0000 L CNN
F 1 "Conn_01x17" H 8950 900 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x17_P2.54mm_Vertical" H 8800 1800 50  0001 C CNN
F 3 "~" H 8800 1800 50  0001 C CNN
F 4 "DNI" H 8750 2850 50  0001 C CNN "DNI"
F 5 "40-0518-10" H 8800 1800 50  0001 C CNN "Manufacturer PN"
F 6 "Aries Electronics" H 8800 1800 50  0001 C CNN "Manufacturer"
F 7 "A460-ND" H 8800 1800 50  0001 C CNN "Digikey PN"
F 8 "" H 8800 1800 50  0001 C CNN "Descrition"
F 9 "CONN SOCKET SIP 40POS GOLD" H 8800 1800 50  0001 C CNN "Description"
	1    8800 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 1000 8100 1000
Text GLabel 8100 1100 0    50   Input ~ 0
+3V3
Wire Wire Line
	8100 1100 8600 1100
$Comp
L Mechanical:Fiducial FID3
U 1 1 5E70F549
P 2350 1800
F 0 "FID3" H 2435 1846 50  0000 L CNN
F 1 "Fiducial" H 2435 1755 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 2350 1800 50  0001 C CNN
F 3 "~" H 2350 1800 50  0001 C CNN
	1    2350 1800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID2
U 1 1 5E708863
P 2350 1450
F 0 "FID2" H 2435 1496 50  0000 L CNN
F 1 "Fiducial" H 2435 1405 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 2350 1450 50  0001 C CNN
F 3 "~" H 2350 1450 50  0001 C CNN
	1    2350 1450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID1
U 1 1 5E707DC6
P 2350 1100
F 0 "FID1" H 2435 1146 50  0000 L CNN
F 1 "Fiducial" H 2435 1055 50  0000 L CNN
F 2 "Fiducial:Fiducial_1mm_Mask2mm" H 2350 1100 50  0001 C CNN
F 3 "~" H 2350 1100 50  0001 C CNN
	1    2350 1100
	1    0    0    -1  
$EndComp
Connection ~ 1300 1300
Wire Wire Line
	1300 1100 1300 1300
Wire Wire Line
	1450 1100 1300 1100
Connection ~ 1300 1500
Wire Wire Line
	1300 1300 1300 1500
Wire Wire Line
	1450 1300 1300 1300
Connection ~ 1300 1700
Wire Wire Line
	1300 1500 1300 1700
Wire Wire Line
	1450 1500 1300 1500
Wire Wire Line
	1300 1700 1300 1800
Wire Wire Line
	1450 1700 1300 1700
$Comp
L tail-rescue:TestPoint-Connector MH4
U 1 1 5E586ED6
P 1450 1700
F 0 "MH4" V 1450 1888 50  0000 L CNN
F 1 "Mounting Hole" V 1495 1888 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1650 1700 50  0001 C CNN
F 3 "~" H 1650 1700 50  0001 C CNN
	1    1450 1700
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:TestPoint-Connector MH3
U 1 1 5E586AF5
P 1450 1500
F 0 "MH3" V 1450 1688 50  0000 L CNN
F 1 "Mounting Hole" V 1495 1688 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1650 1500 50  0001 C CNN
F 3 "~" H 1650 1500 50  0001 C CNN
	1    1450 1500
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:TestPoint-Connector MH2
U 1 1 5E5865A5
P 1450 1300
F 0 "MH2" V 1450 1488 50  0000 L CNN
F 1 "Mounting Hole" V 1495 1488 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1650 1300 50  0001 C CNN
F 3 "~" H 1650 1300 50  0001 C CNN
	1    1450 1300
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:TestPoint-Connector MH1
U 1 1 5E584D96
P 1450 1100
F 0 "MH1" V 1450 1288 50  0000 L CNN
F 1 "Mounting Hole" V 1495 1288 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1650 1100 50  0001 C CNN
F 3 "~" H 1650 1100 50  0001 C CNN
	1    1450 1100
	0    1    1    0   
$EndComp
$EndSCHEMATC
