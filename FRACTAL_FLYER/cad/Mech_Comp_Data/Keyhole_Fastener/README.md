# Information on "keyhole" fasteners.

There are several companies that manufacture similar components. We have decided to use the parts made by PEM Engineering. The PEM Engineering web page with information about these fasteners is [located here](https://catalog.pemnet.com/keyword/?searchType=PartNoSearch&keyword=SKC-F&plpver=1018&key=all&keycateg=100&SchType=1).

The .STEP file located here is an AP214 format .STEP file.

There are at least two (2) local distributors that stock PEM Engineering parts :
* Bossard, Inc. in Milpitas, CA   1-800-289-2779
* D.B. Roberts Company in Santa Clara, CA 1-800-838-4662 (www.dbroberts.com)

The D.B. Roberts has price and availability for the SKC-F parts on their web site. As of January 2020 the pricing was :

|  Qty.  |   Price   |
| :----: | :-------: |
  1  |  0.2811  |
 100 |  0.2595  |
 500 |  0.2270  |
 