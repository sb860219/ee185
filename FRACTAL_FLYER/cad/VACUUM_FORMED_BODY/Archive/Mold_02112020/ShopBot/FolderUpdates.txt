
Feb11 - 2020

- Updated the V6Mold
	Offset the babylip sketch .075" inwards. This makes the cross-section dimension to be 7.71", 
		more than enough to allow the plastic to flow. 

- Updated the Assembly
	Changed the origin of the CAM so it is easy to measure
		

