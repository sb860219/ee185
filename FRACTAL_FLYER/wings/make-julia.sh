src/geomandel -m --image-file output    \
    --image-pnm-grey -col-algo 0         \
    --grey-base 255 --grey-freq 128      \
    --set-color 255,255,255             \
    --width 3000 --height 1000          \
    --bailout 500 --fractal=2          \
    --julia-real -0.4 --julia-ima -0.59 \
    --creal-min -0.60 --creal-max -0.30   \
    --cima-min -0.33 --cima-max -0.23
