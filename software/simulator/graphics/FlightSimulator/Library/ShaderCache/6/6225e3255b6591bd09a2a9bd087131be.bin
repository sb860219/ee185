<Q                         UNITY_HDR_ON    e  ���(      4                          xlatMtlMain #include <metal_stdlib>
#include <metal_texture>
using namespace metal;

#if !(__HAVE_FMA__)
#define fma(a,b,c) ((a) * (b) + (c))
#endif

#ifndef XLT_REMAP_O
	#define XLT_REMAP_O {0, 1, 2, 3, 4, 5, 6, 7}
#endif
constexpr constant uint xlt_remap_o[] = XLT_REMAP_O;
struct FGlobals_Type
{
    float4 _ColorRamp_ST;
    float _Glossiness;
    float _Metallic;
    float _Blend;
    float _BumpPower;
    float _Hue;
    float _Saturation;
    float _Brightness;
    float _Contrast;
};

struct Mtl_FragmentIn
{
    float4 TEXCOORD0 [[ user(TEXCOORD0) ]] ;
    float2 TEXCOORD1 [[ user(TEXCOORD1) ]] ;
    float4 TEXCOORD2 [[ user(TEXCOORD2) ]] ;
    float4 TEXCOORD3 [[ user(TEXCOORD3) ]] ;
    float4 TEXCOORD4 [[ user(TEXCOORD4) ]] ;
    float3 TEXCOORD5 [[ user(TEXCOORD5) ]] ;
};

struct Mtl_FragmentOut
{
    float4 SV_Target0 [[ color(xlt_remap_o[0]) ]];
    float4 SV_Target1 [[ color(xlt_remap_o[1]) ]];
    float4 SV_Target2 [[ color(xlt_remap_o[2]) ]];
    float4 SV_Target3 [[ color(xlt_remap_o[3]) ]];
};

fragment Mtl_FragmentOut xlatMtlMain(
    constant FGlobals_Type& FGlobals [[ buffer(0) ]],
    sampler sampler_MainTex [[ sampler (0) ]],
    sampler sampler_ColorRamp [[ sampler (1) ]],
    sampler sampler_Mask [[ sampler (2) ]],
    sampler sampler_BumpMap [[ sampler (3) ]],
    texture2d<float, access::sample > _MainTex [[ texture(0) ]] ,
    texture2d<float, access::sample > _BumpMap [[ texture(1) ]] ,
    texture2d<float, access::sample > _Mask [[ texture(2) ]] ,
    texture2d<float, access::sample > _ColorRamp [[ texture(3) ]] ,
    Mtl_FragmentIn input [[ stage_in ]])
{
    Mtl_FragmentOut output;
    float3 u_xlat0;
    float3 u_xlat1;
    float3 u_xlat2;
    float3 u_xlat3;
    float u_xlat4;
    float u_xlat5;
    float u_xlat6;
    float u_xlat18;
    u_xlat0.x = dot(input.TEXCOORD5.xyz, input.TEXCOORD5.xyz);
    u_xlat0.x = rsqrt(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * input.TEXCOORD5.xyz;
    u_xlat1.xyz = _BumpMap.sample(sampler_BumpMap, input.TEXCOORD1.xy).xyw;
    u_xlat1.x = u_xlat1.z * u_xlat1.x;
    u_xlat1.xy = fma(u_xlat1.xy, float2(2.0, 2.0), float2(-1.0, -1.0));
    u_xlat18 = dot(u_xlat1.xy, u_xlat1.xy);
    u_xlat18 = min(u_xlat18, 1.0);
    u_xlat18 = (-u_xlat18) + 1.0;
    u_xlat18 = sqrt(u_xlat18);
    u_xlat1.z = u_xlat18 / FGlobals._BumpPower;
    u_xlat18 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat18 = rsqrt(u_xlat18);
    u_xlat1.xyz = float3(u_xlat18) * u_xlat1.xyz;
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat1.xyz);
    u_xlat0.xy = fma(u_xlat0.xx, FGlobals._ColorRamp_ST.xy, FGlobals._ColorRamp_ST.zw);
    u_xlat0.xyz = _ColorRamp.sample(sampler_ColorRamp, u_xlat0.xy).xyz;
    u_xlat2.xyz = _Mask.sample(sampler_Mask, input.TEXCOORD0.zw).xyz;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat2.xyz;
    u_xlat2.xyz = (-u_xlat2.xyz) + float3(1.0, 1.0, 1.0);
    u_xlat3.xyz = _MainTex.sample(sampler_MainTex, input.TEXCOORD0.xy).xyz;
    u_xlat2.xyz = u_xlat2.xyz * u_xlat3.xyz;
    u_xlat0.xyz = max(u_xlat0.xyz, u_xlat2.xyz);
    u_xlat2.xyz = u_xlat0.zxy * float3(0.57735002, 0.57735002, 0.57735002);
    u_xlat2.xyz = fma(u_xlat0.zxy, float3(0.57735002, 0.57735002, 0.57735002), (-u_xlat2.zxy));
    u_xlat18 = FGlobals._Hue * 6.28318548;
    u_xlat4 = sin(u_xlat18);
    u_xlat5 = cos(u_xlat18);
    u_xlat2.xyz = u_xlat2.xyz * float3(u_xlat4);
    u_xlat2.xyz = fma(u_xlat0.xyz, float3(u_xlat5), u_xlat2.xyz);
    u_xlat0.x = dot(float3(0.57735002, 0.57735002, 0.57735002), u_xlat0.xyz);
    u_xlat0.x = u_xlat0.x * 0.57735002;
    u_xlat6 = (-u_xlat5) + 1.0;
    u_xlat0.xyz = fma(u_xlat0.xxx, float3(u_xlat6), u_xlat2.xyz);
    u_xlat0.xyz = u_xlat0.xyz + float3(-0.5, -0.5, -0.5);
    u_xlat18 = fma(FGlobals._Brightness, 2.0, -1.0);
    u_xlat2.xy = float2(FGlobals._Saturation, FGlobals._Contrast) + float2(FGlobals._Saturation, FGlobals._Contrast);
    u_xlat0.xyz = fma(u_xlat0.xyz, u_xlat2.yyy, float3(u_xlat18));
    u_xlat0.xyz = u_xlat0.xyz + float3(0.5, 0.5, 0.5);
    u_xlat18 = dot(u_xlat0.xyz, float3(0.389999986, 0.589999974, 0.109999999));
    u_xlat0.xyz = (-float3(u_xlat18)) + u_xlat0.xyz;
    u_xlat0.xyz = fma(u_xlat2.xxx, u_xlat0.xyz, float3(u_xlat18));
    u_xlat0.xyz = (-u_xlat3.xyz) + u_xlat0.xyz;
    u_xlat0.xyz = fma(float3(FGlobals._Blend), u_xlat0.xyz, u_xlat3.xyz);
    u_xlat18 = fma((-FGlobals._Metallic), 0.779083729, 0.779083729);
    output.SV_Target0.xyz = float3(u_xlat18) * u_xlat0.xyz;
    u_xlat0.xyz = u_xlat0.xyz + float3(-0.220916301, -0.220916301, -0.220916301);
    output.SV_Target1.xyz = fma(float3(FGlobals._Metallic), u_xlat0.xyz, float3(0.220916301, 0.220916301, 0.220916301));
    output.SV_Target0.w = 1.0;
    output.SV_Target1.w = FGlobals._Glossiness;
    u_xlat0.x = dot(input.TEXCOORD2.xyz, u_xlat1.xyz);
    u_xlat0.y = dot(input.TEXCOORD3.xyz, u_xlat1.xyz);
    u_xlat0.z = dot(input.TEXCOORD4.xyz, u_xlat1.xyz);
    u_xlat18 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat18 = rsqrt(u_xlat18);
    u_xlat0.xyz = float3(u_xlat18) * u_xlat0.xyz;
    output.SV_Target2.xyz = fma(u_xlat0.xyz, float3(0.5, 0.5, 0.5), float3(0.5, 0.5, 0.5));
    output.SV_Target2.w = 1.0;
    output.SV_Target3 = float4(0.0, 0.0, 0.0, 1.0);
    return output;
}
                                 FGlobals0   	      _ColorRamp_ST                            _Glossiness                      	   _Metallic                           _Blend                       
   _BumpPower                          _Hue                         _Saturation                   $      _Brightness                   (   	   _Contrast                     ,             _MainTex                  _BumpMap                _Mask                
   _ColorRamp                  FGlobals           