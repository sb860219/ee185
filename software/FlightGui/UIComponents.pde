import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.swing.JFileChooser;
import model.*;
import engine.*;

/**
 * The FLIGHT GUI window that lets you move where a Fractal Flyer is
 * physically placed and save the current physical configuration.
 *
 * The configuration is saved to and loaded from a JSON file whose
 * name is defined by Config.FLYER_CONFIG_FILE.  When the file is
 * saved, the previous one is saved as a backup.
 *
 * Physical placement is first determined which "face" the flyer is
 * placed on: C is hanging from the ceiling pipes, F is along the
 * front window, E is the external edge of the central stair, and I is
 * the internal edge of the central stair. Depending on which face the
 * flyer is placed on, different parameters are available for
 * placement. For example, if you are hanging a flyer from the
 * ceiling, you specify which pipe; if you are hanging it off the from
 * window, you specify which vertical bar.
 *
 *
 *  @author Philip Levis <pal@cs.stanford.edu>
 *
 */

class FlyerLayoutControlWindow extends UIWindow {
  final static int WIDTH     = 200;
  final static int HEIGHT    = 420;
  final static int BORDER    =   4;
  final static int ROW_HEIGHT = 28;
  private UIButton enableButton;
  private UIIntegerBox flyerSelect;
  private UISlider  hangDistanceSlider; // All faces
  private UISlider  tiltSlider; // All faces
  private UISlider  directionSlider; // All faces
  private UISlider xSlider; // Front window: x along row
  private UIToggleSet yToggle; // Front window: which row
  private UISlider  positionSlider; // Spirals: point along spiral
  private UIToggleSet  pipeToggle; // Ceiling: which pipe
  private UISlider  mountPointSlider; // Ceiling: were on pipe


  private UIToggleSet faceToggle;
  private UILabel name;
  private UILabel text;

  void updateEditableParameters() {
      try {
          String face = faceToggle.getSelectedOption();
          // Every mounting point has a hang distance
          hangDistanceSlider.setVisible(true);
          tiltSlider.setVisible(true);
          directionSlider.setVisible(true);
          
          // Other measurements are face-dependent; only make relevant
          // ones visible.
          xSlider.setVisible(false);
          yToggle.setVisible(false);
          pipeToggle.setVisible(false);
          positionSlider.setVisible(false);
          mountPointSlider.setVisible(false);
          
          if (face.equals("C")) {
              pipeToggle.setVisible(true);
              mountPointSlider.setVisible(true);
          } else if (face.equals("F")) {
              xSlider.setVisible(true);
              yToggle.setVisible(true);
          } else if (face.equals("I")) {
              positionSlider.setVisible(true);
          } else if (face.equals("E")) {
              positionSlider.setVisible(true);
          } else {
              System.err.println("Unrecognized face: " + face + "; disable all controls.");
          }
      } catch (NullPointerException ex) {
        // There's something wrong with the initialization sequence.
        // Sometimes this is called before all of the different UI
        // elements have been initialized, so a NPI is thrown.
        // Just catch it here - it only happens at startup.
          System.out.println("Caught NPE at startup -- this is normal and recoverable, but would be good to eventually make this bug go away. -pal");
      }
  }


  FlyerLayoutControlWindow(UI ui) {
      super(ui, "LAYOUT TOOL",
            BORDER, FlightGui.this.height - (HEIGHT + BORDER + FlightGui.BOTTOM_ROW_HEIGHT),
            WIDTH, HEIGHT);

    makeFlyerSelect();
    makeEnableButton();
    makeFaceToggle();
    makeTiltSlider();
    makeDirectionSlider();
    makeHangLengthSlider();
    makeXSlider();
    makeYToggle();
    makePositionSlider();
    makePipeToggle();
    makeMountingSlider();

    name = new UILabel(0, 0, 0, 16);
    name.setLabel("?");

    text = new UILabel(0, 0, 0, 16);
    text.setLabel("?");

    float yPos = TITLE_LABEL_HEIGHT;

    yPos += ROW_HEIGHT;

    yPos = labelRow(yPos, "FLYER #", flyerSelect);
    yPos = labelRow(yPos, "NAME", name);
    yPos = labelRow(yPos, "TEXT", text);
    yPos = labelRow(yPos, "FACE", faceToggle);
    yPos = labelRow(yPos, "HANG", hangDistanceSlider);
    yPos = labelRow(yPos, "TILT", tiltSlider);
    yPos = labelRow(yPos, "DIR", directionSlider);
    yPos = labelRow(yPos, "X", xSlider);
    yPos = labelRow(yPos, "Y", yToggle);
    yPos = labelRow(yPos, "POS", positionSlider);
    yPos = labelRow(yPos, "PIPE", pipeToggle);
    yPos = labelRow(yPos, "MOUNT", mountPointSlider);

    makeSaveButton(yPos);

    setFlyer();
    updateEditableParameters();
    setDescription("Tool for physically laying out Fractal Flyers in Packard stairwell");
  }

  private void makeEnableButton() {
    this.enableButton = new UIButton(4, TITLE_LABEL_HEIGHT, width-8, 20) {
      public void onToggle(boolean enabled) {
        if (enabled) {
          flyerSelect.focus(new MouseEvent(this, 0, 0, 0, 0, 0, 0, 0));
          // When you enable, flash the selected flyer;
          // otherwise, it only flashes when the value changes.
          // If it only flashes when the value changes, then the
          // first time you enable, the selected Flyer flashes,
          // but it doesn't on subsequent enables.
          flyerHighlighter.trigger();
          updateEditableParameters(); 
        }
      }
    };
    this.enableButton.setInactiveLabel("Disabled");
    this.enableButton.setActiveLabel("Enabled");
    this.enableButton.setParameter(flyerHighlighter.enabled);
    this.enableButton.addToContainer(this);
    this.enableButton.setDescription("Enable/disable placement tool");
  }

  private void makeFlyerSelect() {
    this.flyerSelect = new UIIntegerBox().setParameter(flyerHighlighter.flyerIndex);

    LXParameterListener selectListener = new LXParameterListener() {
            public void onParameterChanged(LXParameter parameter) {
                setFlyer();
            }
    };
    flyerHighlighter.flyerIndex.addListener(selectListener);
    this.flyerSelect.setDescription("Which flyer is being placed");
  }

  private void makeFaceToggle() {
    this.faceToggle = new UIToggleSet() {
      protected void onToggle(String value) {
        flyerHighlighter.getConfig().getFaceConfig().face = value;
        updateEditableParameters();
        flyerHighlighter.reloadModel();
      }
    };

    // We have to convert to intern() because of a bug in ToggleSet
    // that compares with == rather than .equals(); if you don't
    // use .intern(), then strings don't match. -pal
    int length = Config.FACE_NAMES.length;
    String options[] = new String[length];
    for (int i = 0; i < length; i++) {
        options[i] = Config.FACE_NAMES[i].intern();
    }
    this.faceToggle.setOptions(options);
  }

  private void makeHangLengthSlider() {
    BoundedParameter hang = new BoundedParameter("Hang", 0, 8 * Geometry.FEET);
    this.hangDistanceSlider = new UISlider(0, 0, 0, 16) {
            public void onParameterChanged(LXParameter parameter) {
                flyerHighlighter.getConfig().getFaceConfig().hangDistance = parameter.getValuef();
                flyerHighlighter.reloadModel();
            }
        };
    this.hangDistanceSlider.setParameter(hang);
  }

  private void makeTiltSlider() {
    BoundedParameter tilt = new BoundedParameter("Tilt", -10, 10);
    this.tiltSlider = new UISlider(0, 0, 0, 16) {
            public void onParameterChanged(LXParameter parameter) {
                flyerHighlighter.getConfig().tilt = parameter.getValuef();
                flyerHighlighter.reloadModel();
            }
        };
    this.tiltSlider.setParameter(tilt);
  }
    
  private void makeDirectionSlider() {
      BoundedParameter direction = new BoundedParameter("Rotation", 0, 360);
    this.directionSlider = new UISlider(0, 0, 0, 16) {
            public void onParameterChanged(LXParameter parameter) {
                flyerHighlighter.getConfig().rotation = parameter.getValuef();
                flyerHighlighter.reloadModel();
            }
        };
    this.directionSlider.setParameter(direction);
  }
    
  private void makeXSlider() {
    BoundedParameter x = new BoundedParameter("X", 0, Geometry.FRONT_WINDOW_WIDTH);
    this.xSlider = new UISlider(0, 0, 0, 16) {
            public void onParameterChanged(LXParameter parameter) {
                flyerHighlighter.getConfig().getFaceConfig().x = parameter.getValuef();
                flyerHighlighter.reloadModel();
            }
        };
    this.xSlider.setParameter(x);
  }

  private void makeYToggle() {
    this.yToggle = new UIToggleSet() {
            protected void onToggle(String str) {
                flyerHighlighter.getConfig().getFaceConfig().y = Integer.parseInt(str);
                flyerHighlighter.reloadModel();
            }
        };
    String[] yOptions = {"1".intern(), "2".intern(), "3".intern(), "4".intern(), "5".intern()};
    this.yToggle.setOptions(yOptions);
  }

  private void makePositionSlider() {
    BoundedParameter pos = new BoundedParameter("Position", 0, 0, 1);
    this.positionSlider = new UISlider(0, 0, 0, 16) {
            public void onParameterChanged(LXParameter parameter) {
                flyerHighlighter.getConfig().getFaceConfig().position = parameter.getValuef();
                flyerHighlighter.reloadModel();
            }
        };
    this.positionSlider.setParameter(pos);
  }


  private void makeMountingSlider() {
    BoundedParameter mount = new BoundedParameter("Mount", 0, 0, 1);
    this.mountPointSlider = new UISlider(0, 0, 0, 16) {
            public void onParameterChanged(LXParameter parameter) {
                flyerHighlighter.getConfig().getFaceConfig().mountPoint = parameter.getValuef();
                flyerHighlighter.reloadModel();
            }
        };
    this.mountPointSlider.setParameter(mount);
  }

  private void makePipeToggle() {
    this.pipeToggle = new UIToggleSet() {
            protected void onToggle(String str) {
                flyerHighlighter.getConfig().getFaceConfig().pipe = Integer.parseInt(str);
                flyerHighlighter.reloadModel();
            }
        };
    String[] pipeOptions = {"0".intern(), "1".intern(), "2".intern(), "3".intern(), "4".intern(), "5".intern(), "6".intern(), "7".intern()};
    pipeToggle.setOptions(pipeOptions);
  }

  // Makes a row in the window, consisting of a label and an assocated
  // UI element.
  private float labelRow(float yPos, String label, UI2dComponent obj) {
    UILabel lobj = new UILabel(4, yPos, 50, 20)
        .setLabel(label);
    lobj.addToContainer(this);
    obj
    .setPosition(58, yPos)
    .setSize(width-62, 20)
    .addToContainer(this);
    yPos += ROW_HEIGHT;
    return yPos;
  }

  private void makeSaveButton(float yPos) {
    UIButton saveButton = new UIButton(4, yPos, this.width-8, 20) {
      public void onToggle(boolean active) {
        if (active) {
          String backupFileName = Config.FLYER_CONFIG_FILE + ".backup." + month() + "." + day() + "." + hour() + "." + minute() + "." + second();
          saveStream(backupFileName, Config.FLYER_CONFIG_FILE);
          FlyerConfig[] configs = model.getFlyerConfigs();
          pengine.getIO().saveConfigFile(flyerConfigurations, Config.FLYER_CONFIG_FILE);
          setLabel("Saved. Restart needed.");
        }
      }
    };
    saveButton.setMomentary(true).setLabel("Save Changes").addToContainer(this);
    saveButton.setDescription("Save current physical layout to " + Config.FLYER_CONFIG_FILE + " and make a backup of the previous layout");
  }
    
  private void setFlyer() {
    FlyerConfig config = flyerConfigurations[flyerHighlighter.flyerIndex.getValuei()];
    hangDistanceSlider.getParameter().setValue(config.getFaceConfig().hangDistance);
    tiltSlider.getParameter().setValue(config.tilt);
    directionSlider.getParameter().setValue(config.rotation);
    xSlider.getParameter().setValue(config.getFaceConfig().x);
    String yVal = Integer.toString(config.getFaceConfig().y).intern();
    yToggle.setSelectedOption(yVal);
    faceToggle.setSelectedOption(config.getFaceConfig().face.intern());
    positionSlider.getParameter().setValue(config.getFaceConfig().position);
    pipeToggle.setSelectedOption(Integer.toString((int)config.getFaceConfig().pipe));
    mountPointSlider.getParameter().setValue(config.getFaceConfig().mountPoint);
    name.setLabel(config.getMetadata().name);
    text.setLabel(config.getMetadata().text);
    updateEditableParameters();
  }
}

class MasterControl extends UIWindow {
  final static int WIDTH = 200;
  final static int HEIGHT = 95;
  final static int BORDER = 4;
  public UIKnob HueKnob;
  public UIKnob SatKnob;
  public UIKnob BriKnob;
  public UIKnob SpeedKnob;
  private UIButton enableButton;
  
  MasterControl(UI ui) {
      super(ui, "Master", BORDER, 200, WIDTH, HEIGHT);
      makeHueKnob();
      HueKnob.setVisible(true);
      makeSatKnob();
      SatKnob.setVisible(true);
      makeBriKnob();
      BriKnob.setVisible(true);
      makeSpeedKnob();
      SpeedKnob.setVisible(true);
      makeEnableButton();
      enableButton.setVisible(true);
  }
  private void makeSpeedKnob() {
    BoundedParameter rate = new BoundedParameter("Speed 0-100",100, 0, 100);
    this.SpeedKnob = new UIKnob(0, 0, 0, 16) {
            public void onParameterChanged(LXParameter parameter) {
				speedVal = parameter.getValuef();
            }
        };
    this.SpeedKnob.setParameter(rate);
    this.SpeedKnob
    .setPosition(135, 30)
    .setSize(20, 20)
    .addToContainer(this);
  }
  private void makeSatKnob() {
    BoundedParameter rate = new BoundedParameter("Sat 0-100",100, 0, 100);
    this.SatKnob = new UIKnob(0, 0, 0, 16) {
            public void onParameterChanged(LXParameter parameter) {
				satoffset = parameter.getValuef();
            }
        };
    this.SatKnob.setParameter(rate);
    this.SatKnob
    .setPosition(95, 30)
    .setSize(20, 20)
    .addToContainer(this);
  }
  private void makeBriKnob() {
    BoundedParameter rate = new BoundedParameter("Bri 0-100",100, 0, 100);
    this.BriKnob = new UIKnob(0, 0, 0, 16) {
            public void onParameterChanged(LXParameter parameter) {
				brioffset = parameter.getValuef();
            }
        };
    this.BriKnob.setParameter(rate);
    this.BriKnob
    .setPosition(55, 30)
    .setSize(20, 20)
    .addToContainer(this);
  }
  private void makeHueKnob() {
    BoundedParameter rate = new BoundedParameter("Hue 0-100",0, 0, 360);
    this.HueKnob = new UIKnob(0, 0, 0, 16) {
            public void onParameterChanged(LXParameter parameter) {
				hueoffset = parameter.getValuef();
            }
        };
    this.HueKnob.setParameter(rate);
    this.HueKnob
    .setPosition(15, 30)
    .setSize(20, 20)
    .addToContainer(this);
  }
  private void makeEnableButton() {
    this.enableButton = new UIButton(25, 66, width-50, 20) {
      public void onToggle(boolean enabled) {
			masterOn = enabled;
      }
    };
    this.enableButton.setInactiveLabel("Disabled");
    this.enableButton.setActiveLabel("Enabled");
    this.enableButton.addToContainer(this);
    this.enableButton.setDescription("Enable Master Controls");
}
}


/* Window for the slider that modifies script rate generation. */
class ScriptRateWindow extends UIWindow {
  final static int WIDTH = 200;
  final static int HEIGHT = 85;
  final static int BORDER = 4;
  private UISlider rateSlider;
  private UIButton enableButton;
  
  ScriptRateWindow(UI ui) {
      super(ui, "Script Update Rate (Hz)", BORDER, 100, WIDTH, HEIGHT);
      makeRateSlider();
      rateSlider.setVisible(true);
      makeEnableButton();
      enableButton.setVisible(true);
  }
  
  private void makeRateSlider() {
    BoundedParameter rate = new BoundedParameter("Rate (Hz)", 10, 1, 50);
    this.rateSlider = new UISlider(0, 0, 0, 16) {
            public void onParameterChanged(LXParameter parameter) {
                pengine.setGeneratorRate(parameter.getValue());
            }
        };
    this.rateSlider.setParameter(rate);
    this.rateSlider
    .setPosition(25, 30)
    .setSize(width-50, 20)
    .addToContainer(this);
  }
  
  private void makeEnableButton() {
    this.enableButton = new UIButton(25, 55, width-50, 20) {
      public void onToggle(boolean enabled) {
        pengine.setGeneratorEnabled(enabled);
      }
    };
    this.enableButton.setInactiveLabel("Disabled");
    this.enableButton.setActiveLabel("Enabled");
    this.enableButton.addToContainer(this);
    this.enableButton.setDescription("Enable/disable script generation.");
  }
  
}


/** 
 * A simple wrapper around a UIMixer, sizing it to the right size.
 * This provides the sliders that let you control the "volume"
 * of the different patterns loaded in the channels.
 *
 * @author: Philip Levis <pal@cs.stanford.edu>
 */
class ChannelMixerWindow extends UIWindow {
  final static int WIDTH     = 708;
  final static int HEIGHT    = 180;
  final static int BORDER    =   4;
  private final UIMixer mixer;

    ChannelMixerWindow(LX thislx, UI ui) {
      super(ui, "MIXER",
            300, FlightGui.this.height - (HEIGHT + BORDER + FlightGui.BOTTOM_ROW_HEIGHT),
            WIDTH, HEIGHT);
      this.mixer = new UIMixer(ui, thislx, 0, 0, HEIGHT);
      this.mixer.addToContainer(this);
      // this.mixer.setBackgroundColor(0xFF00FF00);
      // println("CHANNEL STRIP =======>", this.mixer.channelStrips.get(thislx.engine.mixer.getChannel(0)));
      // println("COLOR BEFORE ===>", this.mixer.channelStrips.get(thislx.engine.mixer.getChannel(0)).getBackgroundColor());
      // this.mixer.channelStrips.get(thislx.engine.mixer.getChannel(0)).setBackgroundColor(0xFFFF0000);
      // // this.mixer.channelStrips.get(thislx.engine.mixer.getChannel(0)).setVisible(false);
      // println(this.mixer.getChild(0));
      // this.mixer.getChild(0).setBackgroundColor(0xFFFF0000);
      // println("CHANNEL STRIP =======>", this.mixer.channelStrips.get(thislx.engine.mixer.getChannel(0)));
      // println("COLOR AFTER ===>", this.mixer.channelStrips.get(thislx.engine.mixer.getChannel(0)).getBackgroundColor());
      // println("MIXER:", this);
      // for (Map.Entry<LXAbstractChannel, UIMixerStrip> entry : mixer.channelStrips.entrySet()) {
      //   println(entry.getKey() + " " + entry.getValue());
      // }
      
      final LXBus firstChannel = thislx.engine.mixer.getChannel(0);
      final UIMixerStrip firstUIMixerStrip = mixer.channelStrips.get(firstChannel);
      final UIMixerStripControls controls = firstUIMixerStrip.controls;

      LXParameterListener changeColorListener = new LXParameterListener() {
        public void onParameterChanged(LXParameter p) {
          controls.setBackgroundColor(40);
        }
      };

      thislx.engine.mixer.focusedChannel.addListener(changeColorListener);
      firstChannel.selected.addListener(changeColorListener);
      // Starts focused
      if (((LXChannel)thislx.engine.mixer.getFocusedChannel()).getIndex() == 0) {
        controls.setBackgroundColor(50);
      } else {
        controls.setBackgroundColor(50);
      }
      controls.setFocusBackgroundColor(50);

      this.setDescription("Mixer for controlling which patterns are active, how they are mixed, and their relative strengths. Channel 0 controls wing position.");
    }

    public UIMixer getMixer() {
      return this.mixer;
    }
}

public class ClipPlayingWindow extends UIWindow {
  private final FlightPlaylist flightPlaylist;
  private static final int WIDTH = 210;
  private static final int HEIGHT = 240;
  private static final int BORDER = 4;
  final static int NUM_TRACKS_VISIBLE = 5;
  final static int TRACK_ROW_HEIGHT = 20;
  final static int TRACK_LIST_HEIGHT = NUM_TRACKS_VISIBLE * TRACK_ROW_HEIGHT;
  
  // Store a list of tracks.
  final UIItemList.ScrollList playQueue;
  private int numTracks = 0;
  
  //Start time is stored as a string with the hour [0-23] in the first index and minute [0-59] in the second
  String[] startTimeArray = new String[]{"9", "00"};
  //End time is stored in two string parameters, one for hour [0-24] and one for minute [0-59]
  StringParameter hourEndParam = new StringParameter("hourEnd", "--");
  StringParameter minuteEndParam = new StringParameter("minuteEnd", "--");
  
  boolean startTimeEnable = false;
  boolean loopingToggledOn = false;
  DiscreteParameter loopNum = new DiscreteParameter("loopNum", 0, 0, 1000);
  
    
  ClipPlayingWindow(LX lx, UI ui, FlightPlaylist playlist) {
    super(ui, "TRACKS", FlightGui.this.width-WIDTH-4, FlightGui.this.height - (HEIGHT + BORDER + FlightGui.BOTTOM_ROW_HEIGHT)+10, WIDTH, HEIGHT);

    this.flightPlaylist = playlist;

    println("Current time:", LocalDateTime.now());
    
    float yPos = TITLE_LABEL_HEIGHT;
    float buttonGap = 2;
    float buttonWidth = (this.width-12-buttonGap*5)/8;
    
    final UIButton recButton = new UIButton(6, yPos, this.width - 12, 20) {
            protected void onToggle(boolean active) {
                if (active) {
                    flightPlaylist.startRecording();
                } else {
                    flightPlaylist.stopRecording();
            		selectOutput("Save Recording",  "saveTrack", new File(dataPath("")), ClipPlayingWindow.this);
                }
            }
        };
    recButton
    .setInactiveLabel("RECORD NEW TRACK")
    .setActiveLabel("END RECORDING")
    .setActiveColor(0xcc3333)
    .setDescription("Record and save a new custom track")
    .addToContainer(this);
    
    yPos += 23;

    final UIButton loopingToggleButton = new UIButton(6 + (this.width-12)/2 + 3, yPos, (this.width-12)/2-3, 18){
      protected void onToggle(boolean active){
        flightPlaylist.setTrackLooping(active); 
        if(active){
           loopingToggledOn = true;
         }
         else{
           loopingToggledOn = false;
         }         
      }
    };
    
    final UIButton startTime = new UIButton(6, yPos, (this.width-12)/2-3, 18){
        protected void onToggle(boolean active){
         if(active){
             startTimeEnable = true;
             //toggle on end times
             toggleEndTimes();
             // disable looping
             flightPlaylist.setTrackLooping(false); 
             loopingToggleButton.setEnabled(false);
          }
          else{
            startTimeEnable = false;
            // toggle off end times
            toggleEndTimes();
            // enable looping
            if (loopingToggledOn) {
              flightPlaylist.setTrackLooping(true);
            }
            loopingToggleButton.setEnabled(true);
          }
        }
    };
    startTime
    .setLabel("Enable Start Time")
    .setDescription("Have the playlist start at a specific time")
    .addToContainer(this);
    

    loopingToggleButton
    .setLabel("Loop")
    .setDescription("Enable to have the playlist loop back to the start after it ends")
    .addToContainer(this);
    
    yPos +=20;
    
    final UITextBox startTimeLabel = new UITextBox(6, yPos, 40, 16){
    };
    startTimeLabel
    .setValue("Time:")
    .setBackgroundColor(60)
    .setDescription("Enter the time [hours 0-23] [minutes 0-59] for the playlist to start at")
    .addToContainer(this);
    
    final UITextBox hourStart = new UITextBox(6 + 40 + 2, yPos, 20, 16){
        protected void saveEditBuffer(){
          String value = this.editBuffer.trim();
          if(value.matches("\\d+") && (int(value) < 24)){
            startTimeArray[0] = value;
            setValue(startTimeArray[0]);
          }
          if (startTimeEnable) {
          	// update end times
          	toggleEndTimes();
          }
        }
    };
    hourStart
    .setValue(startTimeArray[0])
    .setEditable(true)
    .enableImmediateEdit(true)
    .setBackgroundColor(15)
    .setDescription("Input an hour for the playlist to start at [0-23]")
    .addToContainer(this);
    
    final UITextBox minuteStart = new UITextBox(6+ 40 + 2*2 + 20, yPos, 20, 16){
        protected void saveEditBuffer(){
          String value = this.editBuffer.trim();
          if(value.matches("\\d+") && (int(value) < 60)){
            if(int(value) < 10) value = "0" + str(int(value));
            startTimeArray[1] = value;
            setValue(startTimeArray[1]);
          }
          if (startTimeEnable) {
          	// update end times
          	toggleEndTimes();
          }
        }
    };
    minuteStart
    .setValue(startTimeArray[1])
    .setEditable(true)
    .enableImmediateEdit(true)
    .setBackgroundColor(15)
    .setDescription("Input a minute for the playlist to start at [0-59]")
    .addToContainer(this);
    
    final UITextBox endTimeLabel = new UITextBox(6 + (this.width-12)/2 + 3, yPos, 40, 16){
    };
    endTimeLabel
    .setValue("Time:")
    .setBackgroundColor(60)
    .setDescription("The time [hours 0-23] [minutes 0-59] that the playlist ends at will be filled here")
    .addToContainer(this);
    
    final UITextBox hourEnd = new UITextBox(6 + (this.width-12)/2 + 3 + 2 + 40, yPos, 20, 16);
    hourEnd
    .setValue(hourEndParam.getString())
    .setBackgroundColor(60)
    .setDescription("The hour the playlist ends at will be listed here [0-23]")
    .addToContainer(this);
    final LXParameterListener hourEndListener = new LXParameterListener() {
            public void onParameterChanged(LXParameter parameter) {
                hourEnd.setValue(hourEndParam.getString());
            }
    };
    hourEndParam.addListener(hourEndListener);
    
    final UITextBox minuteEnd = new UITextBox(6 + (this.width-12)/2 + 3 + 2*2 + 40 + 20, yPos, 20, 16);
    minuteEnd
    .setValue(minuteEndParam.getString())
    .setDescription("The minute the playlist ends at will be listed here [0-59]")
    .setBackgroundColor(60)
    .addToContainer(this);
    final LXParameterListener minuteEndListener = new LXParameterListener() {
            public void onParameterChanged(LXParameter parameter) {
                minuteEnd.setValue(minuteEndParam.getString());
            }
    };
    minuteEndParam.addListener(minuteEndListener); 
    
    yPos += 19;
    
    final List<UIItemList.Item> items = new ArrayList<UIItemList.Item>();
    final UIItemList.ScrollList list = new UIItemList.ScrollList(ui, 1, yPos, this.width - 2, TRACK_LIST_HEIGHT);
    list.setItems(items);
    list.addToContainer(this);
    playQueue = list;
    playQueue.setReorderable(true);
    
    yPos += TRACK_LIST_HEIGHT + 4;
    
    final UIButton plusButton = new UIButton(6, yPos, buttonWidth, 20) {
      protected void onToggle(boolean active) {
        if (active) {
            selectInput("Select a file", "loadTrack", new File(dataPath("")), ClipPlayingWindow.this);
        }
      }
    };
    plusButton
    .setLabel("+")
    .setMomentary(true)
    .setDescription("Add a new track to the playlist")
    .addToContainer(this);
    
    final UIButton minusButton = new UIButton(6 + buttonWidth + buttonGap, yPos, buttonWidth, 20){
    	protected void onToggle(boolean active) {
        	if (active && (numTracks > 0) && (playQueue.getFocusedIndex()>=0)) {
            	flightPlaylist.removeTrack(playQueue.getFocusedIndex());
            	playQueue.removeItem(playQueue.getFocusedItem());
            	numTracks--;
            	//Remove a track
            }
        }
    };
    minusButton
    .setLabel("-")
    .setMomentary(true)
    .setDescription("Remove the currently selected track from the playlist")
    .addToContainer(this);
    
    final UIButton playButton = new UIButton(6 + 2*(buttonWidth + buttonGap), yPos, buttonWidth*2, 20) {
    	protected void onToggle(boolean active) {
        	if (active) {
              if(!startTimeEnable){
              	flightPlaylist.playbackTrack(0);
                playQueue.setFocusIndex(0);
              }
              else{
                flightPlaylist.startAtTime(int(startTimeArray[0]), int(startTimeArray[1])); 
              }
            } else {
                flightPlaylist.stopTrack();
            }
        }
    };
    playButton
    .setInactiveLabel("PLAY")
    .setActiveLabel("STOP")
    .setDescription("Start/stop the playlist")
    .addToContainer(this);
      
    final UITextBox loopButton = new UITextBox(6 + 4*buttonWidth + buttonGap*3, yPos, (buttonWidth*2*3)/4, 20) {
    	protected void saveEditBuffer(){
          String value = this.editBuffer.trim();
          if(int(value) <= 1000){
            loopNum.decrement(1000, false);
            loopNum.increment(int(value), true);
            
            int index = playQueue.getFocusedIndex();
            UIItemList.Item item = playQueue.getFocusedItem();
            int numIterations = int(value) - ((TrackItem)item).getIterations();
            if(numIterations > 0){
              //Increment for the number of iterations
              for(int i = 0; i < numIterations; ++i){
                flightPlaylist.updateLoopCount(index, true);
                ((TrackItem)item).increaseLoopIterations();
              }
            }
            else{
              //Decrement for the number of iterations
              for(int i = 0; i < abs(numIterations); ++i){
                flightPlaylist.updateLoopCount(index, false);
                ((TrackItem)item).decreaseLoopIterations();
              }
            }
            toggleEndTimes();
          }
        }
    };
    loopButton
    .setValue("Loops")
    .setEditable(true)
    .enableImmediateEdit(true)
    .setBackgroundColor(15)
    .setDescription("Enter the number of loops for the currently selected track")
    .addToContainer(this);
    final LXParameterListener loopNumListener = new LXParameterListener() {
            public void onParameterChanged(LXParameter parameter) {
                loopButton.setValue(str(loopNum.getValuei()));
            }
    };
    loopNum.addListener(loopNumListener);
    
    final UIButton upLoopButton = new UIButton(6 + 4*buttonWidth + buttonGap*3 + (buttonWidth*2*3)/4, yPos, (buttonWidth*2*1)/4, 10){
      protected void onToggle(boolean active){
       if(active){  
          int index = playQueue.getFocusedIndex();
          UIItemList.Item item = playQueue.getFocusedItem();
          flightPlaylist.updateLoopCount(index, true);
		  ((TrackItem)item).increaseLoopIterations();
          toggleEndTimes();
          loopNum.decrement(1000, false);
          loopNum.increment(((TrackItem)item).getIterations(), true);
       }
      }
    };
    upLoopButton
    .setLabel("▲")
    .setMomentary(true)
    .setDescription("Increment the number of loops for the currently selected track")
    .addToContainer(this);
    final UIButton downLoopButton = new UIButton(6 + 4*buttonWidth + buttonGap*3 + (buttonWidth*2*3)/4, yPos+10, (buttonWidth*2*1)/4, 10){
      protected void onToggle(boolean active){
       if(active){
          int index = playQueue.getFocusedIndex();
          UIItemList.Item item = playQueue.getFocusedItem();
          flightPlaylist.updateLoopCount(index, false);
		  ((TrackItem)item).decreaseLoopIterations();
          toggleEndTimes();
          loopNum.decrement(1000, false);
          loopNum.increment(((TrackItem)item).getIterations(), true);
       }
      }
    };
    downLoopButton
    .setLabel("▼")
    .setMomentary(true)
    .setDescription("Decrement the number of loops for the currently selected track")
    .addToContainer(this);
    
    final UIButton upButton = new UIButton(6 + 6*buttonWidth + 4*buttonGap, yPos, buttonWidth, 20){
    	protected void onToggle(boolean active) {
        if (active) {
          println("Up pressed.");
          int index = playQueue.getFocusedIndex();
          UIItemList.Item item = playQueue.getFocusedItem();
          if (index != 0) {
            flightPlaylist.moveInQueue(index, true);
            playQueue.moveItem(item, index - 1);
            playQueue.setFocusIndex(index - 1);
          }
          toggleEndTimes();
          println("FlightPlaylist's playQueue:", flightPlaylist.getTracks());
          println("UIComponents' playQueue:", playQueue.getItems().size());
        }
      }
    };
    upButton
    .setLabel("↑")
    .setMomentary(true)
    .setDescription("Move the currently selected track up in the playlist")
    .addToContainer(this);
    
    final UIButton downButton = new UIButton(6 + 7*buttonWidth + 5*buttonGap, yPos, buttonWidth, 20){
     	protected void onToggle(boolean active) {
        	if (active) {
            	//Move a track down
              println("Down pressed.");
              int index = playQueue.getFocusedIndex();
              UIItemList.Item item = playQueue.getFocusedItem();
              if (index != playQueue.getItems().size() - 1) {
                flightPlaylist.moveInQueue(index, false);
                playQueue.moveItem(item, index + 1);
                playQueue.setFocusIndex(index + 1);
              }
              toggleEndTimes();
            println("FlightPlaylist's playQueue:", flightPlaylist.getTracks().size());
            println("UIComponents playQueue:", playQueue.getItems().size());
        }
      }
    };
    downButton
    .setLabel("↓")
    .setMomentary(true)
    .setDescription("Move the currently selected track down in the playlist")
    .addToContainer(this);
  
    yPos += 24;

    new UIButton(6, yPos, this.width - 12, 20) {
      protected void onToggle(boolean active) {
        if (active) {
            selectOutput("Save Track",  "saveSet", new File(dataPath("")), ClipPlayingWindow.this);
        }
      }
    }
    .setMomentary(true)
    .setLabel("Save")
    .setDescription("Save the current playlist to the computer")
    .addToContainer(this);
        
    final LXParameterListener playIndexListener = new LXParameterListener() {
            public void onParameterChanged(LXParameter parameter) {
                playQueue.setFocusIndex(flightPlaylist.playIndex.getValuei());
            }
    };
    flightPlaylist.playIndex.addListener(playIndexListener);
  }

  public void toggleEndTimes() {
    if (startTimeEnable) {
      LocalDateTime relativeStartTime = LocalDate.now()
        .atTime(int(startTimeArray[0]), int(startTimeArray[1]));
      for (UIItemList.Item track : playQueue.getItems()) {
        relativeStartTime = ((TrackItem)track).toggleEndTime(relativeStartTime);
      }
      // set end time box to relativeStartTime 
      hourEndParam.setValue(relativeStartTime.format(DateTimeFormatter.ofPattern("HH")));
      minuteEndParam.setValue(relativeStartTime.format(DateTimeFormatter.ofPattern("mm")));
    } else {
      for (UIItemList.Item track : playQueue.getItems()) {
        ((TrackItem)track).toggleEndTime(null);
      }
      hourEndParam.setValue("--");
      minuteEndParam.setValue("--");
    }
  }
  
  public void saveTrack(File file) {
    if (file != null) {
      flightPlaylist.saveRecordingToTrack(file);
    }
  }
  
  public void saveSet(File file) {
    if (file != null) {
      flightPlaylist.savePlaylistAsTrack(file);
    }
  }
  
  public void loadTrack(File selection) {
    if (selection == null) {
      println("Window was closed or the user hit cancel.");
    } else {
      flightPlaylist.loadTrack(selection);
      int tracksAdded = flightPlaylist.getTracks().size() - numTracks;
      for (int i = 0 ; i < tracksAdded ; i++) {
	    playQueue.addItem(new TrackItem(flightPlaylist.getTracks().get(numTracks), numTracks));
	    numTracks++;            	
      } 
    }
  }
  
    
  public class TrackItem extends UIItemList.Item {

    final Track track;
  	final String filename;
  	final String spacer = "     ";
    final long trackLength;
  	final String length;
    int iterations;
    String endTime;
	
    TrackItem(Track track, int trackIndex) {
      this.track = track;
      this.filename = track.getFilename();
      this.iterations = flightPlaylist.getTrackLoop(trackIndex);
      this.trackLength = track.getTrackLength();
      this.length = formatDuration(this.trackLength);
      long len = this.trackLength * iterations;
      
      int systemHour = LXTime.hour();
      if(startTimeEnable) {
	      systemHour = int(startTimeArray[0]);
	      int currentHour = systemHour + (int)TimeUnit.MILLISECONDS.toHours(len);
	      int systemMinute = LXTime.minute();
	      if(startTimeEnable) systemMinute = int(startTimeArray[1]);
	      int currentMinute = systemMinute + int(TimeUnit.MILLISECONDS.toMinutes(len) -  
	        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(len)));
	      
	    this.endTime = String.format("%02d:%02d:%02d", 
	    currentHour,
	    currentMinute,
	    TimeUnit.MILLISECONDS.toSeconds(len) - 
	    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(len)));
	  } else {
	  	this.endTime = formatDuration(len);
      }
    }

    String getLabel() {
      return this.iterations + "X" + this.spacer + this.filename + this.spacer + this.length + this.spacer + this.endTime;
    }
    
    int getIterations(){
       return iterations; 
    }
    
    // note that we need to call toggleEndTimes after this function is called
    void increaseLoopIterations() {
      this.iterations++;
      redraw();
    }

    // note that we need to call toggleEndTimes after this function is called
    void decreaseLoopIterations() {
      if (this.iterations != 1) {
	      this.iterations--;
	      redraw();
      }
    }
    
    LocalDateTime toggleEndTime(LocalDateTime relativeStartTime) {
    	if (startTimeEnable) {
    		LocalDateTime relativeEndTime = relativeStartTime.plus(Duration.ofMillis(this.trackLength * this.iterations));
    		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    		this.endTime = relativeEndTime.format(formatter);
    		redraw();
    		return relativeEndTime;
    	} else {
    		this.endTime = formatDuration(this.trackLength * this.iterations); 
    		redraw();
    		return null;
    	}
    }
    
    void onFocus(){
      loopNum.decrement(1000, false);
      loopNum.increment(iterations, false);
    }

	String formatDuration(long durationMS) {
		return String.format("%02d:%02d:%02d", 
			TimeUnit.MILLISECONDS.toHours(durationMS),
			TimeUnit.MILLISECONDS.toMinutes(durationMS) -  
			TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(durationMS)),
			TimeUnit.MILLISECONDS.toSeconds(durationMS) - 
			TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(durationMS)));
	}
  }
}

/**
 * Window that lets you choose the pattern for the currently
 * selected channel in the bottom mixer as well as set the parameters
 * of that Pattern. It queries the FlightEngine for the set of available
 * patterns and provides those as options. A pattern's parameters are
 * displayed as sliders. Heavily copied from SQUARED's UIMultiDeck.
 *
 * A significant portion of the logic in this class is to properly
 * handle automation (playing a track). For example, if a track
 * changes the pattern on a channel, we want to reflect that in the GUI.
 * Put another way, these GUI elements can be modified both by user
 * interaction as well as scripts, so we need to observe when things change
 * using listeners.
 *
 * @author Philip Levis <pal@cs.stanford.edu>
 */
class PatternControlWindow extends UIWindow {
  // A bunch of configuration constants.
  final static int WIDTH     = 150;
  final static int HEIGHT    = 500;
  final static int BORDER    = 4;
  final static int SLIDERS = 4;
  final static int SLIDER_HEIGHT = 24;
  final static int SLIDER_SPACING = 40;
  final static int NUM_PATTERNS_VISIBLE = 6;
  final static int PATTERN_ROW_HEIGHT = 40;
  final static int PATTERN_LIST_HEIGHT = NUM_PATTERNS_VISIBLE * PATTERN_ROW_HEIGHT;
  final static int TITLE_LABEL_HEIGHT = 25;

  // Store a list of patterns and a listener for each channel.
  final UIItemList.ScrollList[] patternLists;
  final LXChannel.Listener[] channelListeners;

  // Sliders below the pattern list, for modifying the parameters of
  // the selected pattern.
  final UISlider[] sliders;
  final LX thislx;
    
  PatternControlWindow(LX mylx, UI ui) {
      super(ui, "Patterns",
            FlightGui.this.width - (WIDTH + BORDER), 30,
            WIDTH, HEIGHT);
      int yp = TITLE_LABEL_HEIGHT;
      this.thislx = mylx;
      
      List<LXAbstractChannel> channels = thislx.engine.mixer.getChannels();
      int numChannels = channels.size();
      patternLists = new UIItemList.ScrollList[numChannels];
      channelListeners = new LXChannel.Listener[numChannels];
      // Set up the pattern selection list
      for (int i = 0; i < numChannels; i++) {
          LXChannel channel = (LXChannel)channels.get(i);
          int index = channel.getIndex();
          List<UIItemList.Item> items = new ArrayList<UIItemList.Item>();
          for (LXPattern p: channel.getPatterns()) {
              items.add(new PatternScrollItem(channel, p));
          }
          UIItemList.ScrollList list = new UIItemList.ScrollList(ui, 1, yp, this.width - 2, PATTERN_LIST_HEIGHT);
          list.setItems(items);
          list.setVisible(channel.getIndex() == thislx.engine.mixer.focusedChannel.getValuei());
          list.addToContainer(this);
          patternLists[index] = list;
      }

      // Create the sliders for pattern parameters
      yp += patternLists[0].getHeight();
      sliders = new UISlider[SLIDERS];
      for (int si = 0; si < sliders.length; ++si) {
          int x = BORDER;
          int y = yp + (si * SLIDER_SPACING);
          UISlider slider = new UISlider(x, y, WIDTH - 2 * BORDER, SLIDER_HEIGHT);
          slider.setEditable(true);
          slider.addToContainer(this);
          sliders[si] = slider;
      }

      // Set up the pattern selection. We need to listen to channels in case automation
      // (e.g., a track) changes the pattern on a channel.
      for (int i = 0; i < numChannels; i++) {
          LXChannel channel = (LXChannel)thislx.engine.mixer.getChannel(i); 
          channelListeners[channel.getIndex()] = new LXChannel.AbstractListener() {
              @Override
              public void patternWillChange(LXChannel channel,
                                            LXPattern pattern,
                                            LXPattern nextPattern) {
                  patternLists[channel.getIndex()].redraw();
              }
                  
              @Override
              /* When the pattern for a channel changes (e.g. due a track), then
               *  show this change in the UI. Change the channel name to
               *  the pattern and make that pattern focused in the list. */
              public void patternDidChange(LXChannel channel, LXPattern pattern) {
              		System.out.println("changing pattern");
                  channel.goPattern(pattern);
                  channel.label.setValue(pattern.getClass().getSimpleName());
                  List<LXPattern> patterns = channel.getPatterns();
                  for (int i = 0; i < patterns.size(); ++i) {
                      if (patterns.get(i) == pattern) {
                      System.out.println("pattern index: " + i);
                          patternLists[channel.getIndex()].setFocusIndex(i);
                          break;
                      }
                  }  
                  patternLists[channel.getIndex()].redraw();

                  /* Change the LXParameters associated with the sliders to the parameters of
                   * the newly selected channel. */
                  if (channel.getIndex() == thislx.engine.mixer.focusedChannel.getValuei()) {
                      int pi = 0;
                      for (LXParameter parameter : pattern.getParameters()) {
                          if (pi >= sliders.length) {
                              break;
                          }
                          if (parameter instanceof LXListenableNormalizedParameter) {
                              sliders[pi].setParameter((LXListenableNormalizedParameter)parameter);
                              sliders[pi].setEditable(true);
                              pi++;
                          }
                      }
                      while (pi < sliders.length) {
                          sliders[pi++].setParameter(null);
                      }
                  }
              }
          };
          channel.addListener(channelListeners[channel.getIndex()]);
          channelListeners[channel.getIndex()].patternDidChange(channel, channel.getActivePattern());
      }

      // Registers when the focused channel changes and updates the displayed pattern list to be
      // of that channel.
      thislx.engine.mixer.focusedChannel.addListener(new LXParameterListener() {
              public void onParameterChanged(LXParameter parameter) {
                  try {
                    LXChannel channel = (LXChannel)thislx.engine.mixer.getFocusedChannel();
                    redraw();
                  
                    channelListeners[channel.getIndex()].patternDidChange(channel, channel.getActivePattern());
                  
                    int pi = 0;
                    for (UIItemList.ScrollList patternList : patternLists) {
                        patternList.setVisible(pi == thislx.engine.mixer.focusedChannel.getValuei());
                        pi++;
                    }
                  } catch (ClassCastException ex) {
                    // Some channels in the mixer panel are special ones,
                    // like the master volume; ignore them.
                  }
              }
          });
  
      this.setDescription("Select and configure patterns for the currently selected channel (" + thislx.engine.mixer.getFocusedChannel().getIndex() + ")");
  }

    /**
     * The elements in the list of patterns. This subclass
     * is mostly about giving them the right label based
     * on the underlying class (LXPattern) they wrap around.
     * Heavily copied from SQUARED.
     *
     * @author Philip Levis <pal@cs.stanford.edu>
     */
  private class PatternScrollItem extends UIItemList.Item {

    private final LXChannel channel;
    private final LXPattern pattern;

    private final String label;

    PatternScrollItem(LXChannel channel, LXPattern pattern) {
      this.channel = channel;
      this.pattern = pattern;
      this.label = pattern.getClass().getSimpleName();
    }

    public String getLabel() {
      return this.label;
    }

    public boolean isSelected() {
      return this.channel.getActivePattern() == this.pattern;
    }

    public boolean isPending() {
      return this.channel.getNextPattern() == this.pattern;
    }

    public void onActivate() {
      this.channel.goPattern(this.pattern);
    }

    public void onFocus() {
      this.channel.goPattern(this.pattern);
    }
  }
}
