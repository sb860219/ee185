# FLIGHT: Packard Interactive Light Sculpture Project

This repository stores all of the files, notes, and technical documents
for FLIGHT, the interactive light sculpture for the Packard building at
Stanford Unviersity.  Stanford students
taking EE185, EE285, and CS241, under guidance from course instructors,
engineer an art piece designed by Charles Gadeken. This engineering involves
designing, building, and testing the mechanical, electrical, and software
components of the pieces.

![GUI](images/gui.png) ![Top](images/top.png) ![bottom](images/bottom.png)

This git repository depends on a few submodule for supporting software
and tools. After you clone this repository, go to the root of the
repository and type

```
$ git submodule init
$ git submodule update
```

This will clone the submodules for the DigiKey KiCAD library (needed
for the Fractal Flyer printed circuit board) and for Heronart's
LXStudio package (needed for the GUI that controls and animates
the simulation).

The directory structure:
  - FRACTAL\_FLYER: all technical information on Fractal Flyers, the moving, illuminated objects that form FLIGHT. 76 Fractal Flyers are hung in Packard to form the complete FLIGHT installation.
  - software: the software to control and visualize FLIGHT, including firmware, UI, and simulation.
  - packard: architectural drawings and models of Packard, including its stairwell
  - autumn-19: materials from the Autumn 2019 offering of EE185. This quarter focused on designing FLIGHT and making an initial prototype of a Fractal Flyer.
  - winter-20: materials from the Winter 2020 offering of EE185/EE285/CS241. This quarter focused on making a first version of a Fractal Flyer by developing the body shell, wings, wing mechanism, circuit board on each Flyer, and the LED cross within the body.
  - spring-20: materials from the Spring 2020 offering of EE185/EE285/CS241. This quarter focused on finishing FlightGUI for programming the installation and writing a detailed graphical simulation of FLIGHT.
  - autumn-21: materials from the Autumn 2021 offering of EE185/EE285/CS241. This quarter focused on five projects to finalize the Flyer: the wings, the body shell, the circuit board on each Flyer, the digital signaling to the Flyer board, and the firmware image running on each Flyer.

Further resources:
  - [Fractal Flyer Bill of Materials](https://docs.google.com/spreadsheets/d/17VOOdsOzH8AatJcCMNQgFOtFsnBo5ioCs1vLwKEDTtI/edit?usp=sharing)
  - [Fractal Flyer CAD](https://cad.onshape.com/documents/4d001dce5ea0f3f778952b4f/w/54c62524b05f6caf7f6e1e41/e/8b63bfe51cc0b51b91d19b49)
  - [Fractal Flyer Drawing Tree (out of date)](https://docs.google.com/document/d/1t9YB293jo6p6bc5lDQ5S1L3p2o_mtnMwk6ygtS_yizA/edit)

