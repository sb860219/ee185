# Assignment 1: Understanding LX Studio and LED visualization

*Written by Philip Levis*

**Due: Tuesday, April 14, 2020 at 2:30 PM (before class)**

One big question in every software project is the decision of when to
reuse existing code and when to write new code. If what you need just
plain doesn't exist, the decision is easy. But typically, in any
non-trivial project, there are parts you'll write new and parts you'll
reuse. When you do reuse software, sometimes it isn't exactly right:
you need to tweak it, or adapt it in some way.

In the world of interactive light art installations, LX Studio is a
commonly used and very well regarded piece of software. The
abstraction it presents is of a 3D volume, filled with color. You can
add parameters, functions, and other computations to control that
color volume, and LX Studio provides support to make easy user
interface (UI) elements to mix and select which controls are
active. Each LED in the art piece is a sample point in this volume.
LX Studio samples those points and provides software to turn all those
samples into data you send out to actually control the LEDs.

In this assignment, you'll start to get to know LX Studio and how it
works.  We'll use Charlie's SQUARED as our starting point. In class on
Tuesday we'll talk about how FLIGHT is different than SQUARED and the
basic software architecture we will be following.


## Goals

When you're done with this assignment, you should

  - understand how LX Studio represents color and controls it through Parameters, Modulators, and Components;

  - understand how LX Studio represents a sculpture as a set of points and generates commands to light it up;

  - understand the relationship between LX Studio and Processing;

  - understand how the SQUARED software uses LX Studio and Processing;

  - have added a new control to the SQUARED software to demonstate this knowledge;

  - have read how the UI draws SQUARED on the screen and how the data is sent over a network to a real SQUARED installation.

There are questions interspersed through the sections of the writeup.
To complete the assignment, please email your answers to Phil and
Charlie.

## Getting Started

To do this assignment, you will need to install the SQUARED software,
which includes a version of LX Studio, and Processing, which is used
to visualize SQUARED on your screen.

### SQUARED

To download SQUARED, go to its [GitHub
page](https://github.com/squaredproject/2squared) and use `git` to
clone the repository to your local machine.

If you've never really used `git` before, or have used it but it seems
completely mysterious to you, [please read through my basic git
starter](git.md). It is complicated. It is sophisticated. It is
amazing. It's used to manage tens of thousands of Linux developers
working on thousands of projects in parallel. And we'll use it to
manage our little project because it'll help us make sure we never
lose our work.

When you download SQUARED, you'll get a copy of LX Studio packaged up
in `Trees/code` directory.

### Processing

[Processing](https://processing.org) is a software framework designed
for the visual arts. SQUARED, and LX Studio, use Processing to visualize
the art piece and provide a GUI.

Install a recent (>3.0) version of Processing.

### Running Squared

Follow the instructions for "How to run Big Trees on desktop using
Processing", with one change: *do not checkout the hayes-valley
branch*. Instead, just use the default (`master`) branch. This means
that you can skip the `git checkout hayes-valley` step.

You've already installed Processing and SQUARED so you can just jump
to "Open Trees.pde in Processing 3.0". After a little while, you should
see a visualization like this:

![SQUARED GUI](squared.png)

The GUI has 6 major elements, labeled here:

![SQUARED GUI annotated](squared-ui.png)

Clockwise, they are:

  - *Master Control*: this has parameters for changing the behavior of the
    overall visual behavior, such as changing its color palette, speed, and
    rotation.
  - *Effect Menu*: this shows you the set of different visual effects that
    are part of the software. You use this menu to select which effects
    are available in the mixer. You can also change how the effect interacts
    with other effects -- does it add, subtract, etc.
  - *Loop, record, playback*: this allows you to load, save, and loop
    existing playlists for the piece. A playlist is a series of settings
    (active effects, their parameters) that evolves over time.
  - *Effect mixer*: this mixing panel lets you select which effects
    are active and at what level.
  - *Physical layout control*: this panel allows you to change the physical
    location of each cluster of LEDs, as well as their network address.
    The network address is only needed with a live, real piece. But in the
    visualization, this panel lets you explore how different physical layouts
    might look, or update the visualization to match the real piece.
  - *Output control*: this is where you can control whether the UI
    outputs data to a physical instrallation. The network addresses here
    (e.g., `10.0.0.166`) need to match the ones in the physical layout panel.

Play around with the UI and answer these questions:

  1. What does the HUE master control do? If you set it to 0 or 360,
  is there any difference? Hint: look into
  [HSV](https://en.wikipedia.org/wiki/HSL_and_HSV) color
  representations.  2. What happens if you set an effect to be MLT
  rather than ADD (in the effect menu? Try doing it with the Lightning
  effect to see what's happening.  What does this tell you about how
  colors are being represented? Hint: look into
  [RGB](https://en.wikipedia.org/wiki/RGB_color_model)
  color representations and think about if each color (red, green,
  blue) is being represented as a number 0-255 or a number 0-1.0.


## LX Studio

The SQUARED software is built on top of LX Studio. If you go into the
SQUARED software directory, and look in `Trees/code` you'll see
`P3LX.jar`, which is the Java library for the Processing harness for
LX Studio. That is, it's the code that lets SQUARED invoke LX Studio
from Processing (remember, we loaded `Trees.pde` in Processing
above?).

LX Studio (and Processing) are both written in Java. If you don't know
Java, now is a good time to start learning! While it's a bit dated and
has some rough edges, it's one of the most popular programming
languages today because everything in Android uses Java. Conceptually,
Java was a huge leap forward when it was introduced in 1995. It
invented or established lots of things which we take for granted
today, such as being able to run the same code on computers with
different processors or operating systems. Its one major downside,
though, is that because it was the first to do these things, it made
some mistakes in doing so. Later languages (e.g., C#, Web Assembly)
learned from these mistakes and came up with technically superior and
cleaner approaches. Still, it's pretty good. None of the programming
in this assignment will require a deep understanding of Java.

LX Studio has a [very thin Wiki of documentation on how it
works](https://github.com/heronarts/LXStudio/wiki). Go to the Wiki and
read about Parameters and Modulators.

  1. Looking at the SQUARED GUI again, what are some things that
     you think are likely to be a Parameter? Name three and what kind
     of Parameter you think they are.
  2. Name three places you think a Modulator is being used.

What we called "effects" in the GUI are called `LXPattern` in the LX
Studio software. Let's see how these appear in software.

## The Fade Pattern

In your GUI, load up the Fade pattern. This is a nice, simple
pattern. It cycles through the Hue space of colors. You can see it has
two pameters: SPEED and SMOOTH. Try setting SPEED all the way up, then
bring it down to a reasonable value. Notice what the units are: the
highest setting is 1000, while the largest is much bigger than 4
digits.

  1. What is the SPEED value setting?

With SPEED around 9,000 (so 2/3 set), try playing with SMOOTH.

  1. What is the SMOOTH value setting?

### The Code

As we start looking deeply at the code behind the Fade pattern, you'll
probably want the [LX Studio API reference](https://lx.studio/api)
open and available.

Take a look at the code for this pattern. In the SQUARED repository,
open `Trees/Patterns_KyleFleming.java` in your favorite text editor. Go
down to

```class Fade extends TSPattern {
```

This is 30 or so lines of code that describe the Fade pattern. Notice that
it creates two Parameters:

```
  final BasicParameter speed = new BasicParameter("SPEE", 11000, 100000, 1000, BasicParameter.Scaling.QUAD_OUT);
  final BasicParameter smoothness = new BasicParameter("SMOO", 100, 1, 100, BasicParameter.Scaling.QUAD_IN);
```

They should look familiar from the UI! This is one of the really nice
things about LX Studio: we can create Parameters for our computations
and Patterns, which we use in computations, and LX Studio will
automatically make UI elements for them.

It turns out that SQUARED is based on an older version of LX Studio
than what you'll find in the documentation. The Fade pattern is
creating `BasicParameter` objects, but in the current code API the
equivalent is called `BoundedParameter`. This is a nice example of
incremental improvements in software engineering. What's a
`BasicParameter`? It doesn't tell you anything about what the class
represents. 'Basic' implies that it's the simplest form there is.  But
it isn't -- it has a range, unlike, say, a `BooleanParameter`, which
is only true or false. So changing it name to `BoundedParameter` is
more descriptive and easier for someone to understand: it says what it
is.

Notice that it also creates a `SinLFO`:

```
  final SinLFO colr = new SinLFO(0, 360, speed);
```

What's a SinLFO? Using the [LX Studio API
reference](https://lx.studio/api), navigate to the
`heronarts.lx.modulator` package and then select the `SinLFO`
class. You can see the complete programming API on the right. A SinLFO
is an oscillator that moves between two values (low and high) with a
sinusoidal curve. Recall that in HSV color, Hue is a value of 0 to 360
(degrees), representing where on the color wheel you are. So looking
at this Pattern's source code, we can see that it doesn't fade through
colors linearly. If you look at it again, you can see this: it slows
around red (0 or 360), and moves most quickly through cyan (180).

### Extending Fade Pattern

Let's change the Fade pattern so we can change the range over which
SQUARED fades colors. If your files ever become corrupted or you break
something, you can always use `git checkout` to get a clean copy of
the file as it is in your local database.

Add two new `BasicParameter` to the Fade pattern, called "HIGH" and
"LOW". They should both have a range of 0 to 360, LOW has an initial
value of 0 and HIGH of 360. To understand which argument is low, high,
and the intial value, consult the documentation for
`heronarts.lx.parameter.BoundedParameter`. Note that you can omit the
final parameter (`BasicParameter.Scaling`).

The line `Fade(LX lx)` is the constructor for a Fade object. Be sure
to call `addParameter` to add your two new Parameters.

The `run` method is what LX Studio calls to apply this Pattern to
SQUARED's color space. Read the method:

```
  public void run(double deltaMs) {
    if (getChannel().getFader().getNormalized() == 0) return;
    for (Cube cube : model.cubes) {
      colors[cube.index] = lx.hsb(
        (int)((int)colr.getValuef() * smoothness.getValuef() / 100) * 100 / smoothness.getValuef(),
        100,
        100
      );
    }
  }
```

The first line checks if the fader for this Pattern in the effect
mixer panel has been set to 0.  If so, it just returns and does no
calculation.

  1. Why might the Fade pattern not compute a color if its fader value is set
     to 0? What benefit does this provide?


Modify `run` so that it sets the range of `colr` to be between LOW and
HIGH. Recall that `colr` is a `SinLFO`. Look at the documentation for
`SinLFO`, in particular in the "Methods inherited from class
heronarts.lx.modulator.LXRangeModulator" section. Note that a better
way to do this would be to listen for when changes occur to those
parameters and only change the range when they do, rather than doing
it every iteration. But this approach is simpler and good enough for
now.

All in all, this should be 5 lines of code: two for the new Parameters,
two for adding them in the constructor, and one new line in `run`.

Stop SQUARED in Processing if you haven't already. Go to the SQUARED
directory and run `compile.sh`. Once your code is compiling, re-run
SQUARED.  Stop the playlist in the playback menu and add the Fade
pattern to your mixer. You'll see it now has a LOW and HIGH dials in
its control panel. Play with them -- do they behave as you'd expect?

  1. Take a screenshot of your SQUARED UI with the LOW and HIGH dials
     in the Fade pattern.

## The SQUARED Cubes

You've now seen how the SQUARED software generates color values to
fill into its cubes. You can have multiple Patterns running in parallel,
which are all mixed together using different accumulation approaches:
addition, subtraction, etc. We'll now look into how the Big Tree is
geometrically described, so that LX Studio can draw it in Processing.

If you haven't already, click on your mouse to rotate SQUARED in your
window.  It's a full 3D model, rendered on the screen. Go to the
Physical Layout control window on the left and enable it. You can how
see the individual LED clusters that SQUARED is composed of. There are
48 clusters. These clusters are tied to how a real SQUARED works. On
the [main README.md for SQUARED], scroll down to NDBS Setup. This
discusses how SQUARED is controlled. Each of the 48 clusters is an
Internet connected hardware LED controller. Each LED controller has 16
outputs: these control the 16 different cubes in each cluster. Each
cube has multiple LEDs in it.

Open up `Trees/data/cluster/clusters_bigtree.json`. This is a JSON
(JavaScript Object Notation) file that describes the physical location
of each cluster. Each cluster has 7 fields:

  1. `TreeIndex` is for when there is more than one tree -- in our case there is only 1 so it is always 0.

  1. An IP address (for sending it network data on how to set its
  LEDS).

  1. A 'face', which describes which side of SQUARED the cluster is on. 0-3
  are the four faces, while 4-7 are the four corners. 

  1. A 'level', which specifies how high up the structure the cluster is:
  it is associated with the horizontal bar that clusters are attached to.

  1. An `offset`, which specifies where on the X axis the cluster is. For
  corner faces, it has no meaning.

  1. A `mountPoint`, which is a finer granularity height setting within a level.
  
  1. A `skew`, which specifies how the cluster is rotated.

Play with these fields in the GUI to see how they change the placement of
the cluster.

How is this information brought into the SQUARED software and then rendered
as the Big Tree? Here's the trick I used to find out. First, go to the
top-level SQUARED directory. Then, in your shell,

```
$ grep -R clusters_bigtree.json *
```

This command says "please recursively look for the string
clusters_bigtree.json from every file and directory in this
directory." `grep` is the command for "please look for the string in
files". The `-R` option says do it recursively -- look inside
directories and their subdirectories, etc. Finally `*` means look for
it in every file and directory.

Here's the output I saw:

```
pal@ubuntu:~/src/2squared$ grep -R clusters_bigtree.json *
README.md:* Trees/clusters_bigtree.json --one of the mapped json config, unique for every new squared tree (both big and mini)
README.md:The computer is running Processing 3.0 app and has a json config file that has all the IP addresses of each NDB at data/clusters_bigtree.json
README.md:%G•%@     Top map NDB: Trees/data/clusters_bigtree.json
README.md:o     Hit Save Changes (Trees/data/clusters_bigtree.json)
Binary file Trees/build-tmp/Config.class matches
Binary file Trees/build-tmp/Engine.class matches
Trees/Config.java:  static final String CLUSTER_CONFIG_FILE = "data/clusters_bigtree.json";
```

The important line is the last one -- there's a Java file that declares a
String constant called CLUSTER_CONFIG_FILE that names our file. So this
is how SQUARED knows to look in that file. Now, where is that String used?

```
pal@ubuntu:~/src/2squared$ grep -R CLUSTER_CONFIG_FILE *
Binary file Trees/build-tmp/Config.class matches
Trees/Config.java:  static final String CLUSTER_CONFIG_FILE = "data/clusters_bigtree.json";
Trees/Engine.java:    clusterConfig = loadConfigFile(Config.CLUSTER_CONFIG_FILE);
Trees/UI.pde:          String backupFileName = Config.CLUSTER_CONFIG_FILE + ".backup." + month() + "." + day() + "." + hour() + "." + minute() + "." + second();
Trees/UI.pde:          saveStream(backupFileName, Config.CLUSTER_CONFIG_FILE);
Trees/UI.pde:          engine.saveJSONToFile(clusterConfig, Config.CLUSTER_CONFIG_FILE);
```

In this case, the line we care about is `Trees/Engine.java`: this is the
line of code that loads the configuration file. If open up that file
and look at it, we can see that

```  final List<TreeConfig> clusterConfig;
```

Let's not worry exactly how the file is read in and turned into a
`List<TreeConfig>`. But we can see, on line 73, right after it's created,
that it's used to create a `Model`:

```    model = new Model(clusterConfig);
```

Thankfully, SQUARED has some simple documentation. Open up
`Trees/javadoc/Model.html`. You can see that a Model describes the physical
model of the piece. It's a subclass of an LXModel.

### The UI

So far, this is all about how LX Studio is internally representing the
piece.  It doesn't say how it's visualized. You can run the software
in a "headless" mode, where there's no GUI. The visualization and UI is
where Processing comes in. Open up `Trees/UI.pde` and look through it.

  1. Which line of code draws a cube on the screen?

  1. Which function draws the struts/frame of SQUARED?

### Controlling LEDs

Finally, let's see how the tool generates the commands for LEDs in a
working installation. Recall that a cluster has big and small cubes in
it. The bigger ones have more LEDs in them. So to make a big cube one
color requires sending commands to more LEDs than it does to make a
small cube one color. Recall that the 16 cubes in a cluster each have
their own channel. So when we want to set the cubes in a cluster to
certain colors, we figure out the color of each cube (through LX
studio), then turn that into properly formatted network messages that
tell each NDB what values to send on their 16 channels to their 16
cubes.

If you look inside `Output.java`, you'll see a very simple routine for
generating the data for a cluster:

```
class Output {
  static DDPDatagram clusterDatagram(Cluster cluster) {
    int[] pointIndices = new int[Cluster.PIXELS_PER_CLUSTER];
    int pi = 0;
    for (Cube cube : cluster.cubes) {
      int numPixels = (cube.size >= Cube.LARGE) ? Cube.PIXELS_PER_LARGE_CUBE : Cube.PIXELS_PER_SMALL_CUBE;
      for (int i = 0; i < numPixels; ++i) {
        pointIndices[pi++] = cube.index;
      }
    }
    return new DDPDatagram(pointIndices);
  }
}
```

Each cube is a single point in LX studio. The color value for cube i
is stored in `colors[i]`. The field `cube.index` stores what a cube's
i value is. So what this code does is iterate across all the cubes in
a cluster and stores those indices in a message. If it's a larger
cube, it inserts `PIXELS_PER_LARGE_CUBE` copies of the cube's index
into the message. If it's a smaller cube, it inserts
`PIXELS_PER_SMALL_CUBE` copies of the index into the message.

Note that it doesn't actually put the color into the message! It puts
the index into the color array. LX Studio later takes these indices
and samples the colorspace with them. Then, this message is sent to
the NDB, which sends commands to the actual LED strips.

## Next Steps

You've now done a basic walkthrough of how the SQUARED software works.
If you have any questions, please include them in your writeup. On
Tuesday Charlie and Phil will walk through a software architecture for
how we will design our controller and visualization.

## Handing in

Please send an email to the Charlie and Phl with subject "Assignment
1" which has your answers to all of the above questions. Please send
it in the body of an email: do not send attachments, Google doc links,
etc. If you received significant help from someone besides the course
staff, or worked in a group, please describe the help you
received/your collaboration at the top of your handin.




