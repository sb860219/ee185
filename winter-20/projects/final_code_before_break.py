import time
import board, neopixel, digitalio, pulseio
import busio
import adafruit_lis3dh
import math

MIN_SPEED = 20 #Set to lowest PWM %high that moves wing
MAX_SPEED = 100 #Set to PWM %high that represents max speed we want the wings to move
MOTOR_PWM_FREQUENCY = 20000

#SPI setup:
from digitalio import DigitalInOut, Direction
spi = busio.SPI(board.SCK, board.MOSI, board.MISO)

#right wing accel init
cs_right = DigitalInOut(board.D37)
sensor_right = adafruit_lis3dh.LIS3DH_SPI(spi, cs_right)

#left wing accel init
cs_left = DigitalInOut(board.D36)
sensor_left = adafruit_lis3dh.LIS3DH_SPI(spi, cs_left)

#center plate accel init
cs_center = DigitalInOut(board.D35)
sensor_center = adafruit_lis3dh.LIS3DH_SPI(spi, cs_center)

#body LEDs init
pixel_pin_body = board.D44
num_pixels_body = 237
body_pixels = neopixel.NeoPixel(pixel_pin_body, num_pixels_body, brightness=1.0, auto_write=False)

#right wing LEDs init
pixel_pin_right_wing = board.D43
num_pixels_right_wing = 75
right_wing_pixels = neopixel.NeoPixel(pixel_pin_right_wing, num_pixels_right_wing, brightness=1.0, auto_write=False)

#left wing LEDs init
pixel_pin_left_wing = board.D49
num_pixels_left_wing = 75
left_wing_pixels = neopixel.NeoPixel(pixel_pin_left_wing, num_pixels_left_wing, brightness=1.0, auto_write=False)

#Motor driver init
motor_right_backward = pulseio.PWMOut(board.D41, frequency=MOTOR_PWM_FREQUENCY)
motor_right_forward = pulseio.PWMOut(board.D42, frequency=MOTOR_PWM_FREQUENCY)
motor_left_backward = pulseio.PWMOut(board.D59, frequency=MOTOR_PWM_FREQUENCY)
motor_left_forward = pulseio.PWMOut(board.D60, frequency=MOTOR_PWM_FREQUENCY)


'''Returns the acceeleration of the entire bird (top plate acceleration)'''
def get_bird_angle():
    bird_x, bird_y, bird_z = sensor_center.acceleration

    x_y_accel = abs(bird_x) + abs(bird_y)

    if bird_z == 0:
        bird_z = 0.0001 #Should never happen (bird is sideways?)
    tan_pitch = bird_x / bird_z
    angle_rads = math.atan(tan_pitch)
    angle_degrees = angle_rads * 180 / math.pi
    return (angle_degrees, x_y_accel)

'''Gets the angle of the left wing if left=True, right if left=False, after subtracting out the
acceleration of the entire bird.'''
def get_wing_angle(left):
    bird_angle, _ = get_bird_angle()

    accel_x = 0.0
    accel_y = 0.0
    accel_z = 0.0
    if left:
        accel_x, accel_y, accel_z = sensor_left.acceleration
    else:
        accel_x, accel_y, accel_z = sensor_right.acceleration

    if accel_z == 0:
        accel_z = 0.0001

    tan_pitch = accel_y / accel_z
    angle_rads = math.atan(tan_pitch)

    angle_degrees = angle_rads * 180 / math.pi
    if left:
        angle_degrees += bird_angle
    else:
        angle_degrees -= bird_angle
    return angle_degrees

'''converts an integer percent (0-100) to the u16 that corresponds to a duty cycle that % high'''
def percent_to_duty_cycle(percent):
    return percent * 65535//100

'''Set the direction and speed for the passed motor (left or right)'''
def set_motor_direction_and_speed(left, up, percent):
    if left:
        if up:
            motor_left_forward.duty_cycle = percent_to_duty_cycle(percent)
            motor_left_backward.duty_cycle = 0
        else:
            motor_left_backward.duty_cycle = percent_to_duty_cycle(percent)
            motor_left_forward.duty_cycle = 0
    else:
        if up:
            motor_right_forward.duty_cycle = percent_to_duty_cycle(percent)
            motor_right_backward.duty_cycle = 0
        else:
            motor_right_backward.duty_cycle = percent_to_duty_cycle(percent)
            motor_right_forward.duty_cycle = 0

'''Set the angle of the passed wing to the desired angle in degrees'''
def set_wing_angle(left, degrees):
    cur_angle = get_wing_angle(left)
    diff = abs(cur_angle - degrees)
    speed = 0
    ERR_THRESHOLD_DEGREES = 2 #Margin of error (degrees) within which wing will not be moved
    BEGIN_SLOW_DEGREES = 40 #Rotational distance (degrees) from desired angle at which
                            #the motor should move at less than full speed
    if diff < ERR_THRESHOLD_DEGREES:
        speed = 0
    elif diff < BEGIN_SLOW_DEGREES:
        speed = int(((diff - ERR_THRESHOLD_DEGREES)/(BEGIN_SLOW_DEGREES-1))*(MAX_SPEED-MIN_SPEED) + MIN_SPEED)
    else:
        speed = MAX_SPEED

    if not left:
        #TODO: Remove me once motors matched
        if diff < ERR_THRESHOLD_DEGREES:
            speed = 0
        elif diff < BEGIN_SLOW_DEGREES:
            speed = int(((diff - ERR_THRESHOLD_DEGREES)/(BEGIN_SLOW_DEGREES-1))*(50-15) + 15)
        else:
            speed = 50

    if cur_angle < degrees:
        set_motor_direction_and_speed(left, False, speed)
    else:
        set_motor_direction_and_speed(left, True, speed)

def set_wing_led_pattern(left, pattern):
    if left:
        for i in range(0, LEDS_PER_WING):
            left_wing_lights[i] = pattern[i]
            left_wing_lights.show()
    else:
        for i in range(0, LEDS_PER_WING):
            right_wing_lights[i] = pattern[i]
            right_wing_lights.show()


def wheel(pos):
    # Input a value 0 to 255 to get a color value.
    # The colours are a transition r - g - b - back to r.
    if pos < 0 or pos > 255:
        r = g = b = 0
    elif pos < 85:
        r = int(pos * 3)
        g = int(255 - pos*3)
        b = 0
    elif pos < 170:
        pos -= 85
        r = int(255 - pos*3)
        g = 0
        b = int(pos*3)
    else:
        pos -= 170
        r = 0
        g = int(pos*3)
        b = int(255 - pos*3)
    return (r, g, b)


def rainbow_cycle_step(j, pixels, num_pixels):
    if j > 255:
        j = 0
    pixel_index = (256 // 1) + j
    color = wheel(pixel_index & 255)
    for p in range(num_pixels):
        pixels[p] = color
    pixels.show()
    return j

######################### MAIN LOOP ##############################
start = (int)(time.monotonic())
left_wing_target_angle = 45 #flap left wing this many degrees above/below level
right_wing_target_angle = 45 #flap right wing this many degrees above/below level
period = 12 #Period (s) before reversing target angle
last_passed = -1

body_j = 0
right_wing_j = 125
left_wing_j = 125
idx = 0
while True:
    idx += 1
    passed = (int)(time.monotonic()) - start

    # Pattern LEDs for this step
    # To increase wing responsiveness, program body on even iterations and wings on odd
    if idx % 2 == 0:
        body_j = rainbow_cycle_step(body_j, body_pixels, num_pixels_body) + 2
    else:
        right_wing_j = rainbow_cycle_step(right_wing_j, right_wing_pixels, num_pixels_right_wing) + 2
        left_wing_j = rainbow_cycle_step(left_wing_j, left_wing_pixels, num_pixels_left_wing) + 2

    if passed%period == 0 and passed != last_passed:
        left_wing_target_angle = left_wing_target_angle * -1 #comment in to flap
        right_wing_target_angle = right_wing_target_angle * -1 #comment in to flap
        last_passed = passed

    set_wing_angle(True, left_wing_target_angle)
    set_wing_angle(False, right_wing_target_angle)

    bird_angle, x_y_accel = get_bird_angle()
    MAX_BIRD_ACCEL = 5 #Protection against hits -- freeze motors for 5 seconds
    if x_y_accel > MAX_BIRD_ACCEL:
        set_motor_direction_and_speed(True, True, 0)
        set_motor_direction_and_speed(False, True, 0)
        print("FREEZE")
        time.sleep(5)
    angle_left = get_wing_angle(True)
    angle_right = get_wing_angle(False)
    print('angle left: {0:0.3f}, angle_right: {1:0.3f}, bird_angle: {2:0.3f}, target: {3}'.format(angle_left, angle_right, bird_angle, left_wing_target_angle))


