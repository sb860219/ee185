# Project: Packard Stairway Model

*Instructor Lead: Charles Gadeken*

**Due: Tuesday, February 4, 2020**

The goals and deliverables for this project are:
  1. Make scale models of 100 Fractal Flyers 
  2. Complete the accurate scale model of the Packard stairwell
  3. Make a hanging system to explore layouts of the Fractal Flyers in the stairwell 
  4. Hang some birds

## Make scale models of 100 Fractal Flyers

Purchase a spool of fishing line.

For initial models, laser cut 100 2D kite shapes from thin white plastic.
The geometry of the shape should be the same as the profile of a Fractal
Flyer and to scale with the stairwell. Each shape should have a single
small hole for hanging with fishing line.  This hole should be at the 
center of mass so the piece will hang flat. If you are waiting for
the fishing line, you can size the hole based on the line dimensions.

Extra credit: etch the line between body and wing.

We will explore making 3D models in a later project.

## Complete the accurate scale model and make a hanging system

CAD and cut a back wall. Include the recessed windows as holes. 
If you can, paint it white.

CAD and cut a ceiling panel for the stairwell out of wood. It should 
have a few hundred holes in it for hanging the flyers. These holes should
all be along the back wall and front glass. This may take several tries.
Once the panel seems correct, cut one out of clear acrylic.

## Hang some birds

Take 15-20 birds and hang them. Show the model to the class.



