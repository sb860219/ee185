# Project: Wing Production 

*Instructor Lead: Philip Levis*

**Due: Tuesday, Feb 4th, 2020**

The goals and deliverables for this project are:
  1. Decide and document the wing material thickness, size to purchase, and cutting workflow
  1. Decide on which LED strips to purchase
  1. Design, document, and test the production process for the wings
  1. Write up report and share with class.
  1. When report is approved, order materials needed for the wings

## Wing Material

We know that we want to use the [iridescent acrylic from AliExpress](https://www.aliexpress.com/store/group/Iridescent-Sheets-3-0mm/433980_511757391.html?spm=2114.12010615.pcShopHead_2538105.1_8_1). But it comes in multiple thicknesses
and sizes. Decide which thickness we will use and how large pieces we
want to order. 

The thickness is a tradeoff between fitting to the
casings and cost/weight. Consult with the mechanism group to determine
the weight that could be tolerated (include the LEDs, casing, and
accelerometers). In your report, document these values and your calculations. 
Based on these tradeoffs, make a thickness recommendation
and state your reasoning in your report.

The size will be determined by considering the workflow to cut the pieces.
We want efficiency, both in use of the material and labor.
For material efficiency, We want to use as little material as possible to 
cut wings. For labor efficiency, we want the fewest number of
cutting jobs (more wings in a job is better). Based on these
considerations, in your report make a recommendation on what size panels 
to purchase and state your reasoning.

## LED strips

We will almost certainly want to use SK2813 mini LED strips, because they
fit in the channels and are slightly superior technically to WD2813. Based
on the feedback from the class and Charlie's guidance, decide on an LED
density to use. Explore AliExpress and find an LED strip we should purchase.
Your goal is to select a reliable strip that is cheapest. A reliable
LED provider on AliExpress is [Ray Wu](https://www.aliexpress.com/store/701799).

Document which LED strip we should purchase and state your reasoning
in your report.

## Accelerometer

Decide where and how the accelerometer board will be mounted on the wing.
It should be placed so that its wired connection to the body is alongside
the LED wires and as little of the wing as possible has wires on it. This
may involve making mounting holes on the wing.

## Wiring and Connectors

The LEDs require 3 wires. The accelerometer requires 6 (power, ground,
clock, MISO, MOSI, chip select). Both of these should end in small
connectors with a physical clip, that will plug into the opposite connector
which is in the top plate. For example, the LED strip will have a small
3-pin connector at the end of its wires. In the top plate there will 
be a small hole where the corresponding connector is mounted. Plugging
the wing into the body does not require disassembling the body.

Select the connectors that will be at the end of the accelerometer and
LED strip. Make sure that the corresponding connector on the body can
be mounted on the top plate so it doesn't slip inside and that the
connector has a physical clip that will prevent it from coming loose.

Determine how the wires will be made clean and robust to continuous
wing motion for 5+ years. Example solutions include coiled wires,
shrink wrapping, and having a loop (so rotation just causes the loop
to constrict or expand a bit, rather than put stress on the connectors
and their joints).

## Design, Document, and Test Assembly Process

Now that we know what a wing is, we need to come up with a simple,
safe, fast, precise, and repeatable process for making them. Of
particular concern is the aluminum edging and LEDs of the wing: how they
will be cut, fitted and attached.

For example, each wing requires 3 pieces of channel with precise
(approximately 0.01"/0.5 degree tolerance) lengths and angle cuts. We 
need a way to quickly cut 200+ each of these. Similarly, we want 
the way that the pieces are affixed together to be robust yet easy 
to do hundreds of times. 

If this process requires building fixtures or other tools, build these
and put their CAD drawings in the git repository.

Document this process in your report.

## Report 

Write up your report and share it as a PDF with the class. It should:
 - Recommend what thickness panels and why.
 - Recommend what size panels and why.
 - Recommend which LED strips to purchase and why.
 - Recommend which connectors to use for the LEDs and acceleromter and why.
 - Describe the assembly process for the wings.

## Purchase Materials

When your report has been approved by the instructors, work with
Steve to order the panels, LEDs, casing, and any other materials
we will need to assemble the wings. Commit your report to the
design documents folder of the repository.


