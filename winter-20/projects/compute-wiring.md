# Project: Compute Wiring

*Instructor Lead: Mark Horowitz and Steve Clark*

**Due: Tuesday, January 28st, 2020**

The goals and deliverables for this project are:
  1. Document the wiring topology connecting 100 shapes to a PC over USB as well as power
  2. Determine the connectors used for all wiring points
  3. Compute the maximum power budget allowed per shape by your design
  4. Design, prototype, and build needed PCBs
  5. Build complete wiring setups for 2 boards and test their maximum distance
  6. Demonstrate remote reprogramming (script and firmware) for your 2 boards 

## Document the wiring topology

The wiring topology needs to deliver 3 things to each shape: power, 
USB data, and a reset line.  A shape (its board, motors, and LEDs) operate
at 5 volts and will draw several amps of current. Because several amps requires
very thick wires, we do not deliver 5V directly to the board. Instead,
power is delivered at 48V, then a board does a 48V-to-5V conversion.

The long-distance wiring between the central PC and boards is Cat7 cable.
Cat7 has four twisted shielded pairs. Two pairs carry 48V (power and ground).
One pair is used for USB data (D+ and -). The final pair is used to deliver
a reset signal, driven by GPIO board connected by USB to the central PC 
(e.g., the Numato USB GPIO 128). At each end of the Cat7 cable is a custom
board with an RJ45 jack that breaks out these signals at either the
head (PC) or tail (embedded board on shape). Take a look at [the wiring
design document](../design-docs/wiring.pdf) to see how this works.

Document the wiring topology the installation will use to deliver power
and data to 100 shapes. The key questions to answer are:
  - What is the USB topology?
  - How are the reset lines and power wired to the head boards?
  - Exactly how is the reset signal delivered to the reset pin?
  - How many 48V power supplies will be used?

## Wiring point connectors

Now that you know the wiring topology, you need to decide exactly how
all the wires are connected. In particular:
  - How will power be connected from the supplies to the head boards?
  - How will the reset lines be connected from the GPIO board to the head boards?
  - How will 5V power be delivered to the board from the tail board?
  - How will reset be delivered to the board from the tail board? Please note that reset involves connecting the reset pin to ground.
  - How will you get USB data to the board from the tail board?

Document your decisions, including part numbers as appropriate. After this
point, someone should be able to take what you've documented and build it
without talking with you.

## Compute power budget

Compute, based on your wire gauge, wire length, conversion losses, and 
fan-out from power supplies, the maximum power you can deliver to each shape.
Talk with the mechanism and wing LED groups to learn what their power
requirements are. Add the board's requirements. If their power requirements
are above what you can deliver, we will have a cross-team design discussion
to figure out the right way forward.

## Design and build PCBs

Design and build the head and tail PCBs. Test them.

## Build complete wiring setups

Purchase any other equipment you might need for your full wiring setup
(e.g., a 48V supply). Using a USB hub, head board, tail board, Cat7
cable, any extension cables and other wires you need, build two complete
wiring setups. Test that you are able to power and reprogram a board
over Cat7 cable. Measure how long a cable (up to 100ft.) you can support.

If the cable is much shorter than you expect, debug the system: it could
be USB delay, USB signal integrity, or other issues. You can force the
board into low speed mode by changing the firmware if needed.

## Demonstrate remote reprogramming

Once you have debugged your complete wiring setup, demonstrate that you can
install new Python scripts onto two boards at the same time (both plugged 
in) by accessing them as USB storage devices. Demonstrate that you can reset
them. Demonstrate you can double-tape reset to update their firmware.

