# Project: Hanging Hardware on a Fractal Flyer

*Instructor Lead: Charles Gadeken*

**Due: Tuesday, Feb 18th, 2020**

The goals and deliverables for this project are:
  1. Decide on how a Fractal Flyer's top plate will be hung from a cable
  1. Design the structure and parts of the mechanism
  1. Build a prototype of the mechanism and test it
  1. Commit your CAD models and materials to the git repository

## Requirements

The way that a Fractal Flyer is suspended from a cable is one of the most
safety critical aspects of this project. If this fails, one can fall and
hurt, even kill, someone. At the same time, we will need to take Fractal
Flyers down to repair them. So we have a few requirements:

 - A Fractal Flyer must not become disconnected from its cable even in a 7.0 earthquake, or after 25 years of 
   installation.
 - Removing a Fractal Flyer from its cable must not require any tools.
 - Removing a Fractal Flyer must not remove any loose parts that can be dropped (e.g., screws).
 - You must be able to adjust the pitch of the Fractal Flyer.
 - It must be possible to make the Fractal Flyer top plate parallel to the ground (it must be possible to balance it).
 - The materials hanging the Fractal Flyer must not be flammable, and a fire within the body must not cause the top plate to be come disconnected from the cable.

## Hanging From a Cable

The end of the cable will be a carabiner or other spring-loaded attachment mechanism. The connection point
to the Flyer will be a ring: to disconnect the Flyer and take it down, you take the carabiner out of the ring.
This ring must attach to the top plate (e.g., through cables) so that it can hold the Flyer up. 

In this step, design how this will work. There might be 2-3 attachment points, which connect to the ring.
You want it so that it's easy to make these the correct length: a few millimeters off and the Flyer might
not hang level. Be sure that your attachment points won't interfere with mounting points on the top
plate, e.g. for motors, hinges, or the LED plate.

Make a CAD model of your design. Get a very conservative weight estimate of a flyer. The top plate,
hinges, and shafts are especially important, as they are some of the heaviest elements. Think about how
you will actually attach to the top plate and how secure these fastenings will be.

## Build a Prototype

Using a current model of a top plate, build 1 or 2 prototypes of your design. This will give you a sense
of how hard it will be to make precisely. Iterate and improve your design as you discover issues. Reach
out to Charlie for ideas if you are stuck. Check with Steve if you want guidance on how to make your
mechanism as safe as possible.

## Commit Design to Repository

Once you have a prototype design that you think is worth a full class review, commit your CAD files
to the git repository and update the drawing tree with the materials that will be needed. Present
your design to the class.



